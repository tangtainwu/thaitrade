package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.GoodsPicture;

public interface GoodsPictureDAO extends BaseMapper<GoodsPicture> {
}
