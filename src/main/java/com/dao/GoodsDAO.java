package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Goods;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
public interface GoodsDAO extends BaseMapper<Goods> {
}
