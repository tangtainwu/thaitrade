package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.GoodsPictureKh;

public interface GoodsPictureKhDAO extends BaseMapper<GoodsPictureKh> {
}
