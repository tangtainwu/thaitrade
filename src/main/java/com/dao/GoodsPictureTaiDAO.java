package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.GoodsPictureTai;

public interface GoodsPictureTaiDAO extends BaseMapper<GoodsPictureTai> {
}
