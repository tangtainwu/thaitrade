package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Baseguige;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
public interface BaseguigeDAO extends BaseMapper<Baseguige> {
}
