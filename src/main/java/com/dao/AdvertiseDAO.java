package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Advertise;

public interface AdvertiseDAO extends BaseMapper<Advertise> {
}
