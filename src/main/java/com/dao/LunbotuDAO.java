package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Lunbotu;

/**
 * @Author: ttw
 * @Date: 2021-10-6 11:05
 * @Description:
 */
public interface LunbotuDAO extends BaseMapper<Lunbotu> {
}
