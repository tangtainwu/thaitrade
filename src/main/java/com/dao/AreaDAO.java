package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Area;

public interface AreaDAO extends BaseMapper<Area> {
}
