package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pojo.Chat;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: ttw
 * @Date: 2021-10-11
 * @Description:
 */
public interface ChatDAO extends BaseMapper<Chat> {
    IPage getZuijinChat(Page p,@Param("kefuname") String kefuname);
    Chat getChat(@Param("username") String username,@Param("areaId") String areaId);
}
