package com.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Chatdetail;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: ttw
 * @Date: 2021-10-10
 * @Description:
 */
public interface ChatdetailDAO extends BaseMapper<Chatdetail> {
}
