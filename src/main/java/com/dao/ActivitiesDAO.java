package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pojo.Activities;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: ttw
 * @Date: 2021-10-7 10:30
 * @Description:
 */
public interface ActivitiesDAO extends BaseMapper<Activities> {
    IPage getpage(Page p, @Param("title")String title,@Param("status") int status);

    Activities getByid(@Param("id") String id);
}
