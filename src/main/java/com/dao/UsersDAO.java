package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Admin;
import com.pojo.Users;

/**
 * @Author: Liao
 * @Date: 2021-10-19 10:57
 * @Description:
 */
public interface UsersDAO extends BaseMapper<Users> {
}
