package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.GoodsPictureEn;

public interface GoodsPictureEnDAO extends BaseMapper<GoodsPictureEn> {
}
