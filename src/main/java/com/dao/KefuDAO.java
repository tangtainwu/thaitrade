package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Kefu;

/**
 * @Author: ttw
 * @Date: 2021-10-10
 * @Description:
 */
public interface KefuDAO extends BaseMapper<Kefu> {
}
