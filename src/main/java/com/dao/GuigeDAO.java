package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Guige;

/**
 * @Author: ttw
 * @Date: 2021-10-6 11:02
 * @Description:
 */
public interface GuigeDAO extends BaseMapper<Guige> {
}
