package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Goods;
import com.pojo.GoodsType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@Mapper
public interface GoodsTypeDAO extends BaseMapper<GoodsType> {

    @Select("select distinct superior_id from goods_type where superior_id is not null ORDER BY superior_id")
    @ResultType(List.class)
    List<String> selectparent_ID();

    @Select("select id,gname,superior_id,createTime,grade from goods_type where superior_id is null")
    List<GoodsType> selectNoneparent();

    List<GoodsType> selectparent();

    @Select("select id,gname,superior_id,createTime from goods_type where superior_id = any (select distinct superior_id from goods_type ORDER BY superior_id)")
    List<GoodsType> selectSubordinate();

    @Select("select * from goods GROUP BY(gTypeId) ORDER BY sum(gSellnum) DESC LIMIT 4")
    List<Goods> searchSigeYijiType();

}
