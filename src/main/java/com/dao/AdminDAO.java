package com.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pojo.Admin;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
public interface AdminDAO extends BaseMapper<Admin> {
}
