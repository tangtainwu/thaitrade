package com.util;
import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.Map;

public class SmsUtil {
    private static  String msgHost = "";    // 主机地址
    private static   String appCode = "";  // 阿里云appcode
    private static    String templateID = "";   // 短信模板id
   private static  String path = "";        // 接口地址
   private static  String method = "POST";

   public static void init(String h,String acode,String pa,String temp){
       msgHost = h;
       appCode = acode;
       templateID = temp;
       path = pa;
   }

    public static boolean send(String tel,String code){

        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appCode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
        Map<String, String> bodys = new HashMap<String, String>();
//        bodys.put("callbackUrl", "http://test.dev.esandcloud.com");
        bodys.put("channel", "0");
        bodys.put("mobile", tel);
        bodys.put("templateID", templateID);
        bodys.put("templateParamSet", code);
        System.out.println(appCode+"-----------appcode");
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(msgHost, path, method, headers, querys, bodys);
            if("200".equals(response.toString().substring(9,12)))
                return true;
            else
                return  false;
            //获取response的body
//            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }
}
