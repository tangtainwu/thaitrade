package com.util;

import cn.hutool.core.util.HexUtil;
import cn.hutool.crypto.symmetric.DES;
import org.bouncycastle.jcajce.provider.asymmetric.util.DESUtil;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;

public class StringUtil {

    public static boolean isNull(String str){
        return str == null || str.trim().equals("") || str.equals("undefined") || str.equals("null") ;
    }

    public static boolean isNotNull(String str){
        return !isNull(str) ;
    }

    public static boolean isNumber(String str){
        if(isNull(str)) {
            return false ;
        }
        str = str.trim() ;
        return str.matches("\\d+(\\.\\d+)?") ;
    }

    public static boolean isNotNumber(String str){
        return !isNumber(str) ;
    }


    //DES算法
    public static byte[] desCrypto(byte[] datasource, byte[] password) {
        try{
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(password);
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            //正式执行加密操作
            return cipher.doFinal(datasource);
        }catch(Throwable e){
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(HexUtil.encodeHex(new DES(HexUtil.decodeHex("7F19AE3990000000")).encrypt(HexUtil.decodeHex("0000000000000000"))));
    }
}
