package com.config;

import com.alibaba.fastjson.JSONObject;
import com.bean.ResponseBean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginInterceptor implements HandlerInterceptor {


    /**
     * 在请求处理之前进行调用（Controller方法调用之前）
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        try {
            if (request.getSession().getAttribute("admin") != null) {
                return true;
            }else{
                if (request.getSession().getAttribute("kefu") != null){
                    return true;
                }else{
                    response.setContentType("text/html;charset=utf-8");
                    ResponseBean responseBean = new ResponseBean(403, "登陆失效", null);
                    String str = JSONObject.toJSONString(responseBean);
                    try {
                        response.getWriter().println(str);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            response.setContentType("text/html;charset=utf-8");
            ResponseBean responseBean = new ResponseBean(403, "登陆失效", null);
            String str = JSONObject.toJSONString(responseBean);
            try {
                response.getWriter().println(str);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        } finally {

        }
        return false;//如果设置为false时，被请求时，拦截器执行到此处将不会继续操作,true时，会继续操作
    }

    /**
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    /**
     * 在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    }
}