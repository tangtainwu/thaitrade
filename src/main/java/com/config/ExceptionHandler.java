package com.config;

import com.bean.ResponseBean;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseBean handleException(Exception ex) {
        ex.printStackTrace();
        String msg = ex.getMessage();
        return new ResponseBean(500,"0","出错了,"+ex);
    }


}
