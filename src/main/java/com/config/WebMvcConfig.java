package com.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     全局配置一次性解决每一个方法上都去加注解未免太麻烦了
     全局配置只需要在配置类中重写addCorsMappings方法
     表示本应用的所有方法都会去处理跨域请求，
     allowedMethods表示允许通过的请求数,
     allowedHeaders则表示允许的请求头。
     ，就不必在每个方法上单独配置跨域了
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
//        //  /**表示拦截所有请求
//        String[] addPath = {"/**"};
//        // 放行getStudent和getUser两个请求：其中getUser请求没有对应的处理器
//        String[] excludePath = {"/getStudent","/getUser"};
//        registry.addInterceptor(new InterceptorByInterface()).addPathPatterns(addPath).excludePathPatterns(excludePath);
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("*");
    }

    /**
     * 增加图片转换器
     * @param converters
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new BufferedImageHttpMessageConverter());
    }

//   @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        // 定义jwt拦截器的规则
//        registry.addInterceptor(new JwtFilter()).addPathPatterns("/**")
//                .excludePathPatterns("/api/guke/v1/loginByPhone")  // 使用手机号码登录
//                .excludePathPatterns("/api/guke/v1/regist")        // 注册接口
//                .excludePathPatterns("/api/guke/v1/checkPhone")    // 检验手机号码是否注册
//                .excludePathPatterns("/api/v1/code")               // 发送验证码
//                .excludePathPatterns("/api/guke/v1/loginByOpenId") // 使用openId登录
//                .excludePathPatterns("/api/v1/upload")             // 文件上传接口
//                .excludePathPatterns("/api/shiji/v1/shijiCodeCheck")     // 检验验证码是否正确
//                .excludePathPatterns("/tmp/**")                     // 放行静态资源
//                .excludePathPatterns("/images/**")                  //
//                .excludePathPatterns("/api/shiji/v1/regist")        // 注册
//                .excludePathPatterns("/api/shiji/v1/login")         // 司机登录
//                .excludePathPatterns("/api/shiji/v1/test")
//                .excludePathPatterns("/wx/check")
//                ;
//    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(new LoginInterceptor());
        registration.addPathPatterns("/admin/**","/goods/**","/activities/**","/chat/**","/goodsImages/**","/admin/GoodsType/**","/guige/**","/kefu/**","/lunbotu/**","/sms/**","/admin/upload/**","/upload.do/**","/websocket/**");                      //设置被拦截的路径，主要是后台的请求接口
        registration.excludePathPatterns(                         //添加不拦截路径
                "/admin/login",          //登录
                "/kefu/kefuLogin",       //客服登录
                "/**/*.html",            //html静态资源
                "/**/*.js",              //js静态资源
                "/**/*.css",             //css静态资源
                "/**/*.woff",
                "/**/*.ttf",
                "/goods/getGoodsPageByType",
                "/goods/allTuijian",
                "/goods/getGoodsDetailById",
                "/chat/getHistoryUser",
                "/goods/searchGoodsPage",
                "/activities/getActivitiesByTime",
                "/lunbotu/getLunbotu",
                "/admin/GoodsType/getGoodsFenlei",
                "/activities/getActivitiesByIdUser",
                "/admin/quitLogin",
                "/admin/quitLogin",
                "/chat/getUserHintNumber",
                "/chat/useHaveReadMessage",
                "/admin/upload/sendimg"
        );
    }
}
