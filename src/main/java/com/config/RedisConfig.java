package com.config;

//import com.listener.RedisMessageListener;
import org.springframework.context.annotation.Configuration;

/**
 * redis监听容器
 * 监听redis过期的key
 * 具体处理com.listener.RedisMessageListener类
 * 还要配置redis.windows-service.conf文件(linux叫redis.conf)中notify-keyspace-events Ex
 * https://blog.csdn.net/sod5211314/article/details/90550670
 *
 * @author lgh
 */
@Configuration
public class RedisConfig {

//    @Autowired
//    private RedisMessageListener messageListener;

//    @Autowired
//    @Qualifier("StringRedisTemplate")
//    private  RedisTemplate  redisTemplate;


//    @Bean("StringRedisTemplate")
//    public RedisTemplate<String, String> getRedisTemplate(RedisConnectionFactory connectionFactory) {
//        StringRedisTemplate redisTemplate = new StringRedisTemplate(connectionFactory);
//        GenericJackson2JsonRedisSerializer jackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
//        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//        redisTemplate.afterPropertiesSet();
//        return redisTemplate;
//    }
//
//    @Bean
//    RedisMessageListenerContainer container(MessageListenerAdapter listenerAdapter) {
//
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(redisTemplate.getConnectionFactory());
//        container.addMessageListener(listenerAdapter, new PatternTopic("__keyevent@0__:expired"));
//        //这里是监听redis第一个库里面key的过期
//        return container;
//    }
//
//    @Bean
//    MessageListenerAdapter listenerAdapter() {
//        return new MessageListenerAdapter(messageListener);
//    }
}