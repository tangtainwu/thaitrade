package com.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedisLockConfig {

    @Value("${spring.redis.host}")
    private  String  host;

    @Value("${spring.redis.port}")
    private  String  port;

    @Value("${spring.redis.password}")
    private  String  password;


    @Bean
    public RedissonClient redissonClient()
    {

        String url = "redis://"+host+":"+port;
        Config config  = new Config();
        if (password!=null && password.trim().length()>0)
        {
            config.useSingleServer().setAddress(url).setPassword(password);
        }
        else
        {
            config.useSingleServer().setAddress(url);
        }
        return Redisson.create(config);
    }
}