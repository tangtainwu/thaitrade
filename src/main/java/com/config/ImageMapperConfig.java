package com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@Configuration
public class ImageMapperConfig implements WebMvcConfigurer {

    @Value("${upload.dir}")
    private String upload;

    @Value("${upload.tmp.dir}")
    private String tmp ;



    @Autowired
    private LoginInterceptor apiInterceptor;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String os = System.getProperty("os.name");
        registry.addResourceHandler("/upload/**").addResourceLocations("file:" + upload).setCachePeriod(31556926) ;
        registry.addResourceHandler("/video/**").addResourceLocations("file:" + upload + "video/").setCachePeriod(31556926);
        registry.addResourceHandler("/images/**").addResourceLocations("file:" + upload + "images/").setCachePeriod(31556926);
        // 临时目录添加访问映射
        registry.addResourceHandler("/tmp/**").addResourceLocations("file:" + tmp ).setCachePeriod(31556926);

        registry.addResourceHandler("/admin/**.html").addResourceLocations("/admin/") ;
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(apiInterceptor).addPathPatterns("/admin/**").excludePathPatterns(Arrays.asList("/admin/**","/app/**", "/admin/**/*.css", "/admin/**/*.js", "/admin/**/*.gif", "/admin/**/*.png", "/admin/**/*.jpg", "/admin/**/*.html"));
    }
}
