package com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

@WebListener
public class TomcatListener implements ServletContextListener {

    private ServletContext servletContext;

    public static String TMP_DIR = null;
    // 物业logo
    @Value("${upload.tmp.dir}")
    private String tmp;

    // 用户头像
    @Value("${upload.dir}")
    // 判断文件夹的路径是否存在
    private String upload ;


    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
//        System.out.println("tomcat关闭了");
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
//        TMP_DIR = arg0.getServletContext().getRealPath("/tmp");
//        System.getProperties().put("tmp_dir", TMP_DIR) ;
//        System.getProperties().put("upload_dir", arg0.getServletContext().getRealPath("/upload")) ;
        File tempFile = new File(tmp);
        if(tempFile.exists()==false) {
            tempFile.mkdirs() ;
        }

        File uploadFile = new File(upload);
        if(!uploadFile.exists()) {
            uploadFile.mkdirs() ;
        }
        servletContext=arg0.getServletContext();

        ApplicationContext applicationContext= WebApplicationContextUtils.getWebApplicationContext(servletContext);
        //SpringUtil.context = applicationContext ;
    }
}