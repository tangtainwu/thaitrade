package com.bean;


import lombok.Data;

@Data
public class ResponseBean {
    private Integer code ;
    private String msg ;
    private Long count ;
    private Object data ;

    public final static ResponseBean OK = new ResponseBean(200 , "success" , "") ;

    public ResponseBean(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResponseBean(Integer code, Long count , Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count ;
    }

    public ResponseBean(Integer code, Integer count , Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count.longValue() ;
    }

    public ResponseBean(Integer code, Long count , String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count ;
    }

    public ResponseBean(Integer code, Integer count , String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count.longValue() ;
    }

    public ResponseBean(){}

    public static ResponseBean error(String msg){
        return new ResponseBean(500 , msg , "") ;
    }

    public static ResponseBean ok(String msg){
        return new ResponseBean(200 , msg , "") ;
    }
}
