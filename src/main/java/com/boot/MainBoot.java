package com.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@ComponentScan({"com.controller","com.service","com.config"})
@MapperScan("com.dao")
@ImportResource("classpath:spring-transaction.xml")
@ServletComponentScan(basePackages = {"com.config"})
@EnableRedisHttpSession(redisNamespace="jinmumianstore:spring:session")//session共享 redis
@EnableSwagger2
public class MainBoot {

    // 独立运行模式
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(com.boot.MainBoot.class);
    }
}
