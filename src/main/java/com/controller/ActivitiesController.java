package com.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bean.ResponseBean;
import com.pojo.Activities;
import com.pojo.Lunbotu;
import com.service.ActivitiesService;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author:TTW
 * @Data: 2021-10-7
 * @Discription:商城活动相关处理
 */
@RestController
@RequestMapping("/activities")
public class ActivitiesController {
    @Autowired
    private ActivitiesService activitiesService;

    /**
     * 新增活动
     * @return
     */
    @RequestMapping("/addActivities")
    public ResponseBean addActivities(@RequestBody Activities act, HttpServletRequest request) throws ParseException {
        try{
            String s=activitiesService.addActivities(act.getTitle(),act.getDetail(),act.getStartTime(),act.getEndTime(),act.getStatus(),act.getTitle_en(),act.getTitle_tai(),act.getTitle_kh(),act.getXianshi(),request.getServerName() + ":" + request.getServerPort(),act.getDetail_en(),act.getDetail_tai(),act.getDetail_kh());
            if (s==null){
                return new ResponseBean(200,"新增成功",null);
            }else{
                return new ResponseBean(500,s,null);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e);
        }
        return new ResponseBean(500,"请检查是否存在非法字符",null);
    }

    /**
     * 分页查询活动列表
     * @param page 页数
     * @param limit 大小
     * @param title 标题
     * @param status 状态
     * @return
     */
    @RequestMapping("/selectActivities")
    public ResponseBean selectActivities(@RequestParam(defaultValue = "1") int page,@RequestParam(defaultValue = "10") int limit,@RequestParam(defaultValue = "") String title,@RequestParam(defaultValue = "-1") int status){
        IPage iPage = activitiesService.getpage(page,limit,title,status);
        return new ResponseBean(0,iPage.getTotal(),"查询成功",iPage.getRecords());
    }

    /**
     * 修改活动的状态
     * @param id 活动id
     * @param status 状态 0或1
     * @return
     */
    @RequestMapping("/updateStatus")
    public ResponseBean updateStatus(String id,Integer status){
        Activities activities=activitiesService.getById(id);
        if (activities==null){
            return new ResponseBean(500,"未找到该活动",id);
        }
        activities.setStatus(status);
        if(activitiesService.updateById(activities)){
            return new ResponseBean(200,"修改成功",activities);
        }else{
            return new ResponseBean(500,"修改失败",activities);
        }
    }

    /**
     * 修改活动是否显示
     * @param id 活动 id
     * @param xianshi 显示状态 0：否 1：是
     * @return
     */
    @RequestMapping("/updateXianshi")
    public ResponseBean updateXianshi(String id,Integer xianshi){
        Activities activities=activitiesService.getById(id);
        if (activities==null){
            return new ResponseBean(500,"未找到该活动",id);
        }
        activities.setXianshi(xianshi);
        if(activitiesService.updateById(activities)){
            return new ResponseBean(200,"修改成功",activities);
        }else{
            return new ResponseBean(500,"修改失败",activities);
        }
    }

    /**
     * 根据id获取活动详情信息
     * @param id 活动id
     * @return
     */
    @RequestMapping("/getActivitiesById")
    public  ResponseBean getActivitiesById(String id){
        Activities activities=activitiesService.getActivitiesById(id,"zh");
        if (activities!=null){
            return new ResponseBean(200,"查询成功",activities);
        }else{
            return new ResponseBean(500,"查询失败",null);
        }
    }

    /**
     * 修改活动的信息
     * @return
     */
    @RequestMapping("/updateActivities")
    public ResponseBean updateActivities(@RequestBody Activities act) throws ParseException {
        try{
            String s=activitiesService.updateActivities(act.getId(),act.getTitle(),act.getDetail(),act.getStartTime(),act.getEndTime(),act.getStatus(),act.getTitle_en(),act.getTitle_tai(),act.getTitle_kh(),act.getXianshi(),act.getDetail_en(),act.getDetail_tai(),act.getDetail_kh());
            if (s==null){
                return new ResponseBean(200,"修改成功",null);
            }else{
                return new ResponseBean(500,s,null);
            }
        }catch (Exception e){
            System.out.println(e);
            e.printStackTrace();
        }
        return new ResponseBean(500,"请检查是否存在非法字符",null);
    }

    /**
     * 获取这个月的活动
     * @param month 月份
     * @param yuyan 语言
     * @return
     */
    @RequestMapping("/getActivitiesByTime")
    public ResponseBean getActivitiesByTime(int month,int year,String yuyan) throws ParseException {
        List<Activities> activitiesList=activitiesService.getActivitiesByTime(month,yuyan,year);
        return new ResponseBean(200,"查询",activitiesList);
    }

    /**
     * 根据id获取活动详情信息
     * @param id 活动id
     * @param yuyan 语言
     * @return
     */
    @CrossOrigin
    @RequestMapping("/getActivitiesByIdUser")
    public  ResponseBean getActivitiesByIdUser(String id,String yuyan){
        Activities activities=activitiesService.getActivitiesById(id,yuyan);
        if (activities!=null){
            return new ResponseBean(200,"查询成功",activities);
        }else{
            return new ResponseBean(500,"查询失败",null);
        }
    }

    /**
     * 删除活动
     * @param id 活动id
     * @return
     */
    @RequestMapping("/delete")
    public ResponseBean delete(String id){
        Activities activities=activitiesService.getById(id);
        if (activities!=null){
            if (activitiesService.removeById(activities.getId())){
                return new ResponseBean(200,"删除成功",null);
            }else{
                return new ResponseBean(500,"删除失败",activities);
            }
        }else{
            return new ResponseBean(500,"未找到该活动",id);
        }
    }

}
