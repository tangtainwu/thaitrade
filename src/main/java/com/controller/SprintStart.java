package com.controller;

import com.dao.SmsConfigDAO;
import com.pojo.SmsConfig;
import com.util.SmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class SprintStart implements ApplicationRunner {

    @Autowired
    private SmsConfigDAO smsConfigDAO ;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        SmsConfig config = smsConfigDAO.selectById(100);
        SmsUtil.init(config.getHost(),config.getAppcode(),config.getPath(),config.getTempletId());

    }
}
