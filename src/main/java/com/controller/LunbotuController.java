package com.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bean.ResponseBean;
import com.pojo.Lunbotu;
import com.service.LunbotuService;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @Author:ttw
 * @Discription:轮播图管理的请求
 */
@RestController
@RequestMapping("/lunbotu")
public class LunbotuController {
    @Autowired
    private LunbotuService lunbotuService;

    @Value("${upload.tmp.dir}")
    private String tmp;

    @Value("${upload.dir}")
    private String upload;

    /**
     * 新增轮播图
     * @param lunbotu
     * @return
     */
    @RequestMapping("/addLunbotu")
    public ResponseBean addLunbotu(@RequestBody Lunbotu lunbotu){
        if(StringUtil.isNull(lunbotu.getPicture())){
            return new ResponseBean(500,1,"请上传图片","");
        }
        int i = lunbotuService.count(new QueryWrapper<Lunbotu>().eq("title",lunbotu.getTitle()));
        if(i>0){
            return new ResponseBean(500,1,"该标题已存在！请重新输入","");
        }
        String images=lunbotu.getPicture();
        lunbotu.setPicture("upload/lunbotu/"+images);
        lunbotu.setCreatetime(new Date());
        if(lunbotuService.save(lunbotu)){
            FileUtil.copy(tmp + images, upload + "lunbotu/"+images, true);
            return new ResponseBean(200,1,"新增成功","");
        }
        return new ResponseBean(500,1,"新增失败","");
    }

    /**
     * 根据ID查询轮播图
     * @param id    轮播图ID
     * @return
     */
    @RequestMapping("/selectLunbotuID")
    public ResponseBean selectLunbotuID(String id){
        return new ResponseBean(0,1,"查询成功",lunbotuService.getById(id));
    }

    /**
     * 查询轮播图
     * @param page      页数
     * @param limit     条数
     * @param title     标题
     * @return
     */
    @RequestMapping("/selectLunbotu")
    public ResponseBean selectLunbotu(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit,String title,@RequestParam(defaultValue = "-1") Integer status,@RequestParam(defaultValue = "-1") Integer sort){
        Page p = new Page(page,limit);
        QueryWrapper<Lunbotu> query = new QueryWrapper();
        if(StringUtil.isNotNull(title)){
            query.like("title",title);
        }
        if (status!=-1){
            query.eq("status",status);
        }
        if (sort!=-1){
            if (sort==1){
                query.orderByAsc("sort");
            }else{
                query.orderByDesc("sort");
            }
        }

        IPage<Lunbotu> iPage = lunbotuService.page(p,query);
        return new ResponseBean(0,iPage.getTotal(),"查询成功",iPage.getRecords());
    }

    /**
     * 修改轮播图状态
     * @param id        轮播图ID
     * @param status    状态
     * @return
     */
    @RequestMapping("/updateLunbotuStatus")
    public ResponseBean updateLunbotuStatus(String id, int status){
        Lunbotu lunbotu = new Lunbotu();
        lunbotu.setId(id);
        lunbotu.setStatus(status);
        if(lunbotuService.updateById(lunbotu)){
            return new ResponseBean(0,1,"操作成功","");
        }
        return new ResponseBean(1,1,"操作失败","");
    }

    /**
     * 修改轮播图
     * @param lunbotu
     * @return
     */
    @RequestMapping("/updateLunbotu")
    public ResponseBean updateLunbotu(@RequestBody Lunbotu lunbotu){
        if(StringUtil.isNull(lunbotu.getPicture())){
            return new ResponseBean(3,1,"请上传图片","");
        }
        int i = lunbotuService.count(new QueryWrapper<Lunbotu>().eq("title",lunbotu.getTitle()));
        int m = lunbotuService.count(new QueryWrapper<Lunbotu>().eq("title",lunbotu.getTitle()).eq("id",lunbotu.getId()));
        if(i>0 && m==0){
            return new ResponseBean(2,1,"该标题已存在！请重新输入","");
        }
        Lunbotu lunbo = lunbotuService.getById(lunbotu.getId());
        String images=lunbotu.getPicture();
        if (images.startsWith("upload")){
            lunbotu.setPicture(images);
        }else{
            lunbotu.setPicture("upload/lunbotu/"+images);
        }
        if(lunbotuService.updateById(lunbotu)){
            if(lunbotu.getPicture().equals(lunbo.getPicture())==false){
                FileUtil.copy(tmp + images, upload + "lunbotu/"+images, true);
            }
            return new ResponseBean(0,1,"修改成功","");
        }
        return new ResponseBean(1,1,"修改失败","");
    }

    /**
     * 客户端获取轮播图
     * @return
     */
    @CrossOrigin
    @RequestMapping("/getLunbotu")
    public ResponseBean getLunbotu(){
        List<Lunbotu> lunbotus = lunbotuService.list(new QueryWrapper<Lunbotu>().eq("status",1).orderByAsc("sort"));//排序
        return new ResponseBean(200,lunbotus.size(),"查询成功",lunbotus);
    }

    /**
     * 删除轮播图
     * @param id 根据id删除轮播图
     * @return
     */
    @RequestMapping("/deleteLunbotu")
    public ResponseBean deleteLunbotu(String id){
        Lunbotu lunbotu=lunbotuService.getById(id);
        if (lunbotu!=null){
            if (lunbotuService.removeById(lunbotu.getId())){
                return new ResponseBean(200,"删除成功",null);
            }else{
                return new ResponseBean(500,"删除失败",lunbotu);
            }
        }else{
            return new ResponseBean(500,"未找到轮播图",id);
        }
    }
}
