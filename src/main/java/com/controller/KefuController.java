package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bean.ResponseBean;
import com.pojo.Admin;
import com.pojo.Kefu;
import com.service.KefuService;
import com.util.MD5Util;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: ttw
 * @Date: 2021-10-10
 * @Description: 客服相关接口处理
 */
@RestController
@RequestMapping("/kefu")
public class KefuController {
    @Autowired
    private KefuService kefuService;

    /**
     * 客服登录
     *
     * @param kefuName 客服名字
     * @param password 密码
     * @return
     */
    @RequestMapping("/kefuLogin")
    public ResponseBean kefuLogin(String kefuName, String password, HttpServletRequest request) {
        if (StringUtil.isNull(kefuName) || StringUtil.isNull(password)) {
            return ResponseBean.error("客服名或者密码不能为空");
        }

        Kefu kefu = kefuService.getOne(new QueryWrapper<Kefu>().eq("kefuName", kefuName).eq("password", MD5Util.getMD5(password)));
        if (kefu != null) {
            if (kefu.getStatus() == 0) {
                return new ResponseBean(500, "该客服已被禁用", kefu);
            } else {
                request.getSession().setAttribute("kefu", kefu);
                return new ResponseBean(200, "登录成功", kefu);
            }
        } else {
            return new ResponseBean(500, "客服名或密码错误", kefu);
        }
    }

    /**
     * 新增客服
     *
     * @param kefu 客服
     * @return
     */
    @RequestMapping("/addKefu")
    public ResponseBean addKefu(@RequestBody Kefu kefu) {
        if (StringUtil.isNull(kefu.getKefuName()) || StringUtil.isNull(kefu.getPassword())) {
            return new ResponseBean(500, "用户名和密码不能为空", null);
        }
        if (kefuService.getOne(new QueryWrapper<Kefu>().eq("kefuName", kefu.getKefuName())) != null) {
            return new ResponseBean(500,"客服名重复！",kefu);
        }
        String pwd = MD5Util.getMD5(kefu.getPassword());
        kefu.setPassword(pwd);
        if (kefuService.save(kefu)) {
            return new ResponseBean(200, "新增成功", kefu);
        } else {
            return new ResponseBean(500, "新增失败", kefu);
        }
    }

    //判断是否客服登录
    @ResponseBody
    @RequestMapping("/pandeng_kefu")
    public ResponseBean pandeng_kefu(HttpServletRequest request) {
        Kefu kefu = (Kefu) request.getSession().getAttribute("kefu");
        if ( kefu!= null) {
            return new ResponseBean(0,"登录成功",kefu.getKefuName());
        }
        return new ResponseBean(500,"登录失败","登录失败");
    }

    /**
     * 分页查询客服列表
     *
     * @param page     页数
     * @param limit    条数
     * @param kefuName 客服名
     * @param status   状态
     * @return
     */
    @RequestMapping("/selectKefu")
    public ResponseBean selectKefu(int page, int limit, String kefuName, @RequestParam(defaultValue = "-1") int status) {
        IPage iPage = kefuService.getKefuPage(page, limit, kefuName, status);
        return new ResponseBean(0, iPage.getTotal(), "成功", iPage.getRecords());
    }

    /**
     * 修改客服的状态
     *
     * @param id     客服id
     * @param status 状态 0或者1
     * @return
     */
    @RequestMapping("/updateStatus")
    public ResponseBean updateStatus(String id, Integer status) {
        Kefu kefu = kefuService.getById(id);
        if (kefu == null) {
            return new ResponseBean(500, "未找到客服", id);
        }
        kefu.setStatus(status);
        if (kefuService.updateById(kefu)) {
            return new ResponseBean(200, "修改成功", kefu);
        } else {
            return new ResponseBean(500, "修改失败", kefu);
        }
    }

    /**
     * 修改客服名或密码
     *
     * @param id        客服id
     * @param columName 修改的字段
     * @param value     修改的值
     * @return
     */
    @RequestMapping("/edit")
    public ResponseBean edit(String id, String columName, String value) {
        String s = kefuService.edit(id, columName, value);
        if (s == null) {
            return new ResponseBean(200, "修改成功", null);
        } else {
            return new ResponseBean(500, "修改失败", s);
        }
    }

    /**
     * 修改客服密码
     *
     * @param password    旧密码
     * @param newPassword 新密码
     * @param token       客服id
     * @return
     */
    @RequestMapping("/kefuChangePassword")
    public ResponseBean adminChangePassword(HttpServletRequest request, String password, String newPassword, String token) {
        Kefu kefu = kefuService.getById(token);
        if (kefu == null) {
            return ResponseBean.error("登录过时");
        }
        if (kefu == null) {
            ResponseBean.error("未找到客服");
        }
        if (password.equals(kefu.getPassword())) {
            kefu.setPassword(MD5Util.getMD5(newPassword));
            if (kefuService.updateById(kefu)) {
                return ResponseBean.ok("成功");
            }
            return ResponseBean.error("失败");
        }
        return ResponseBean.error("密码错误");
    }

    /**
     * 更换客服的区域
     * @param id 客服id
     * @param areaId 区域id
     * @return
     */
    @RequestMapping("/updateArea")
    public ResponseBean updateArea(String id ,String areaId){
        if (StringUtil.isNull(id) || StringUtil.isNull(areaId)){
            return new ResponseBean(500,"参数不能为空",null);
        }
        Kefu kefu = kefuService.getById(id);
        if (kefu!=null){
            kefu.setAreaId(areaId);
            if (kefuService.updateById(kefu)){
                return new ResponseBean(200,"更改成功",kefu);
            }
            return new ResponseBean(500,"修改失败",null);
        }
        return new ResponseBean(500,"该客服不存在",id);
    }
}
