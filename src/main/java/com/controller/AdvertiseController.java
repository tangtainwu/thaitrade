package com.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bean.ResponseBean;
import com.pojo.Advertise;
import com.pojo.Lunbotu;
import com.service.AdvertiseService;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @Author: ttw
 * @Date: 2022-02-21
 * @Description:
 */
@RestController
@RequestMapping("/advertise")
public class AdvertiseController {
    @Autowired
    private AdvertiseService advertiseService;

    @Value("${upload.tmp.dir}")
    private String tmp;

    @Value("${upload.dir}")
    private String upload;

    /**
     * 新增广告弹窗
     * @param advertise 广告
     * @return
     */
    @PostMapping("/addAdvertise")
    public ResponseBean addAdvertise(@RequestBody Advertise advertise){
        try{
            if (StringUtil.isNull(advertise.getPicture()) || StringUtil.isNull(advertise.getPicture_en()) || StringUtil.isNull(advertise.getPicture_tai())){
                return new ResponseBean(500,"请选择图片",null);
            }
            if (advertise.getStatus()==1){
                int number = advertiseService.getStatusNumbers();
                if (number>0){
                    advertiseService.updateStatus();
                }
            }
            advertise.setCreateTime(new Date());
            if (advertiseService.save(advertise)){
                String zh = advertise.getPicture();
                String en = advertise.getPicture_en();
                String tai = advertise.getPicture_tai();
                String kh = advertise.getPicture_kh();
                FileUtil.copy(tmp + zh, upload + "advertise/"+zh, true);
                FileUtil.copy(tmp + en, upload + "advertise/"+en, true);
                FileUtil.copy(tmp + tai, upload + "advertise/"+tai, true);
                FileUtil.copy(tmp + kh,upload + "advertise/"+kh,true);
                return new ResponseBean(200,"新增成功",advertise);
            }
            return new ResponseBean(500,"新增失败",null);
        }catch (Exception e){
            System.out.println(e);
            e.printStackTrace();
        }
        return new ResponseBean(500,"功能异常",null);
    }

    /**
     * 分页查询
     * @param page 页数
     * @param limit 条数
     * @param status 状态
     * @return
     */
    @RequestMapping("/selectAdvertise")
    public ResponseBean selectAdvertise(@RequestParam(value = "page",defaultValue = "1") Integer page,@RequestParam(value = "limit",defaultValue = "10") Integer limit,@RequestParam(value = "status",defaultValue = "-1") Integer status){
        IPage iPage = advertiseService.getPage(page,limit,status);
        return new ResponseBean(0,iPage.getTotal(),"成功",iPage.getRecords());
    }

    /**
     * 修改广告弹窗
     * @param advertise 广告弹窗
     * @return
     */
    @PostMapping("/updateAdvertise")
    public ResponseBean updateAdvertise(@RequestBody Advertise advertise){
        try{
            if (StringUtil.isNull(advertise.getPicture()) || StringUtil.isNull(advertise.getPicture_en()) || StringUtil.isNull(advertise.getPicture_tai()) || StringUtil.isNull(advertise.getPicture_kh())){
                return new ResponseBean(500,"请选择图片",null);
            }
            Advertise advertiseOld = advertiseService.getById(advertise.getId());
            if (advertiseOld==null){
                return new ResponseBean(500,"该广告不存在",null);
            }
            if (advertise.getStatus()==1){
                if (advertise.getStatus()==1){
                    int number = advertiseService.getStatusNumbers();
                    if (number>0){
                        advertiseService.updateStatus();
                    }
                }
            }
            if (advertiseService.updateById(advertise)){
                String zh = advertise.getPicture();
                String en = advertise.getPicture_en();
                String tai = advertise.getPicture_tai();
                String kh = advertise.getPicture_kh();
                if (advertise.getPicture()!=advertiseOld.getPicture()){
                    FileUtil.copy(tmp + zh, upload + "advertise/"+zh, true);
                }
                if (advertise.getPicture_en()!=advertiseOld.getPicture_en()){
                    FileUtil.copy(tmp + en, upload + "advertise/"+en, true);
                }
                if (advertise.getPicture_tai()!=advertiseOld.getPicture_tai()){
                    FileUtil.copy(tmp + tai, upload + "advertise/"+tai, true);
                }
                if (advertise.getPicture_kh()!=advertiseOld.getPicture_kh()){
                    FileUtil.copy(tmp + kh, upload + "advertise/"+kh, true);
                }
                return new ResponseBean(200,"新增成功",advertise);
            }
        }catch (Exception e){
            System.out.println(e);
            e.printStackTrace();
        }
        return new ResponseBean(500,"功能异常",null);
    }

    /**
     * 修改状态
     * @param id        ID
     * @param status    状态
     * @return
     */
    @PostMapping("/updateAdvertiseStatus")
    public ResponseBean updateLunbotuStatus(String id, Integer status){
        Advertise advertise = new Advertise();
        advertise.setId(id);
        advertise.setStatus(status);
        if (status==1){
            if (advertise.getStatus()==1){
                int number = advertiseService.getStatusNumbers();
                if (number>0){
                    advertiseService.updateStatus();
                }
            }
        }
        if(advertiseService.updateById(advertise)){
            return new ResponseBean(0,1,"操作成功","");
        }
        return new ResponseBean(1,1,"操作失败","");
    }

    /**
     * 删除广告
     * @param id 根据id删除
     * @return
     */
    @PostMapping("/deleteAdvertise")
    public ResponseBean deleteLunbotu(String id){
        Advertise advertise = advertiseService.getById(id);
        if (advertise!=null){
            if (advertiseService.removeById(advertise.getId())){
                return new ResponseBean(200,"删除成功",null);
            }else{
                return new ResponseBean(500,"删除失败",advertise);
            }
        }else{
            return new ResponseBean(500,"未找到轮播图",id);
        }
    }

    /**
     * 前端获取广告图片
     * @param yuyan
     * @return
     */
    @CrossOrigin
    @RequestMapping("/getAdvertise")
    public ResponseBean getAdvertise(String yuyan){
        Advertise advertise = advertiseService.getAdvertise(yuyan);
        if (advertise!=null){
            return new ResponseBean(200,"成功",advertise.getPicture());
        }
        return new ResponseBean(500,"没有广告",null);
    }

}
