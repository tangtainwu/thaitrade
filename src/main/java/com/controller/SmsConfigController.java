package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bean.ResponseBean;
import com.pojo.SmsConfig;
import com.pojo.Users;
import com.service.SmsConfigService;
import com.service.UsersService;
import com.util.SmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Liao
 * @Date: 2021-10-19
 * @Description: 用户相关接口处理
 */
@RestController
@RequestMapping("/sms")
public class SmsConfigController {
    @Autowired
    private SmsConfigService configService;
    /**
     * 获取短信配置
     * @param id 编号
     * @return
     */
    @RequestMapping("/getConfig")
    public ResponseBean getConfig(String id){
        SmsConfig config = configService.getById(id);
        if(config==null)
            return ResponseBean.error("查询失败");
        else
            return  new ResponseBean(200,1,"查询成功",config);
    }

    /**
     * 修改短信配置
     * @param id 编号
     * @return
     */
    @RequestMapping("/updateConfig")
    public ResponseBean updateConfig(String id,String host,String path,String appcode,String templetId){
        SmsConfig config = configService.getById(id);
        config.setAppcode(appcode);
        config.setHost(host);
        config.setPath(path);
        config.setTempletId(templetId);
       boolean f =  configService.updateById(config);
        if(f)
        {
            init(id);
            return ResponseBean.ok("修改成功");
        }
        else
            return ResponseBean.error("修改失败");
    }
    /**
     * 初始化短信接口
     *  @param id 编号
     *
     */

    public void init(String id){
        SmsConfig config = configService.getById(id);
        SmsUtil.init(config.getHost(),config.getAppcode(),config.getPath(),config.getTempletId());
        System.out.println(789789);
    }

}
