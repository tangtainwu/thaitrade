package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bean.ResponseBean;
import com.pojo.Admin;
import com.service.AdminService;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:TTW
 * @Discription:后台登录
 */
@RequestMapping("/admin")
@RestController
public class AdminLoginController {

    @Autowired
    private AdminService adminService;

    /**
     * 平台登录
     * @param username  用户名
     * @param password  密码
     * @param request   转发
     * @return
     */
    @RequestMapping("/login")
    public ResponseBean adminLogin(String username, String password, HttpServletRequest request){
        if(StringUtil.isNull(username) || StringUtil.isNull(password)) {
            return ResponseBean.error("账号或者密码不能为空") ;
        }
//        System.out.println(username+"---"+password);
        Admin admin=adminService.getOne(new QueryWrapper<Admin>().eq("username",username).eq("password",password));
        if (admin!=null){
            request.getSession().setAttribute("admin_id",admin.getId());
            request.getSession().setAttribute("admin" ,admin);
            return new ResponseBean(200,"登录成功",admin);
        }else{
            return new ResponseBean(500,"用户名或密码错误",admin);
        }
    }

    //判断平台是否登录
    @ResponseBody
    @RequestMapping("/pandeng_admin")
    public ResponseBean updateSession(HttpServletRequest request){
        Admin admin = (Admin) request.getSession().getAttribute("admin");
        if(admin != null) {
            return new ResponseBean(0,"登录成功",admin.getUsername());
        }
        return new ResponseBean(500,"登录失败","请先登录");
    }

    /**
     * 修改超级管理员密码
     * @param password      旧密码
     * @param newPassword   新密码
     * @param token         管理员ID
     * @return
     */
    @RequestMapping("/adminChangePassword")
    public ResponseBean adminChangePassword(HttpServletRequest request, String password, String newPassword, String token) {
        Admin admin = (Admin) request.getSession().getAttribute("admin");
        if (admin == null) {
            return ResponseBean.error("登录过时");
        }
//        System.out.println("session用户："+admin);
//        System.out.println("旧密码："+password+" 新密码："+newPassword+" 管理员ID："+token);
        Admin a = adminService.getById(token);
        if(a == null){
            ResponseBean.error("未找到管理员");
        }
        if(password.equals(a.getPassword())){
            admin.setPassword(newPassword);
            if(adminService.updateById(admin)){
                return ResponseBean.ok("成功");
            }
            return ResponseBean.error("失败");
        }
        return ResponseBean.error("密码错误");
    }

    /**
     * 退出admin登录，后台
     * @param request
     * @return
     */
    @RequestMapping("/quitLogin")
    public ResponseBean quitLogin(HttpServletRequest request){
        try{
            request.getSession().removeAttribute("admin");
            request.getSession().removeAttribute("kefu");
            return new ResponseBean(200,"退出成功",null);
        }catch (Exception e){
            return new ResponseBean(500,"系统异常",null);
        }

    }
}
