package com.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bean.ResponseBean;
import com.pojo.Guanyu;
import com.service.GuanyuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 主页关于图片控制层
 */
@RestController
@RequestMapping("/guanyu")
public class GuanyuController {
    @Autowired
    private GuanyuService guanyuService;
    @Value("${upload.dir}")
    private String upload;
    @Value("${upload.tmp.dir}")
    private String tmp;

    /**
     * 获取品牌故事图片
     * @return
     */
    @RequestMapping("/getGuanyu")
    public ResponseBean getGuanyu(){
        List<Guanyu> list = guanyuService.list(new QueryWrapper<Guanyu>().eq("type",1));
        if (list.size()>0){
            return new ResponseBean(200,"查询成功",list.get(0));
        }else{
            return new ResponseBean(0,"未找到",null);
        }
    }

    /**
     * 获取联系我们图片
     * @return
     */
    @RequestMapping("/getGuanyu2")
    public ResponseBean getGuanyu2(){
        List<Guanyu> list = guanyuService.list(new QueryWrapper<Guanyu>().eq("type",2));
        if (list.size()>0){
            return new ResponseBean(200,"查询成功",list.get(0));
        }else{
            return new ResponseBean(0,"未找到",null);
        }
    }

    /**
     * 新增关于的图片
     * @param guanyu
     * @return
     */
    @RequestMapping("/addGunayu")
    public ResponseBean addGunayu(@RequestBody Guanyu guanyu){
        List<Guanyu> list = guanyuService.list(new QueryWrapper<Guanyu>().eq("type",1));
        if (list.size()==0){
            if (guanyu!=null && guanyu.getPicture()!=null && guanyu.getPicture_en()!=null && guanyu.getPicture_th()!=null){
                String img = guanyu.getPicture();
                String img2 = guanyu.getPicture_en();
                String img3 = guanyu.getPicture_th();
                String img4 = guanyu.getPicture_kh();
                guanyu.setPicture("/images/goods/"+img);
                guanyu.setPicture_en("/images/goods/"+img2);
                guanyu.setPicture_th("/images/goods/"+img3);
                guanyu.setPicture_kh("/images/goods/"+img4);
                guanyu .setType(1);
                if (guanyuService.save(guanyu)){
                    FileUtil.copy(tmp + img, upload + "images/goods/" + img, true);
                    FileUtil.copy(tmp + img2, upload + "images/goods/" + img2, true);
                    FileUtil.copy(tmp + img3, upload + "images/goods/" + img3, true);
                    FileUtil.copy(tmp + img4, upload + "images/goods/" + img4, true);
                    return new ResponseBean(200,"新增成功",guanyu);
                }else{
                    return new ResponseBean(500,"新增失败",null);
                }
            }else{
                return new ResponseBean(500,"图片为空",null);
            }
        }else {
            return new ResponseBean(500,"已有图片啊",null);
        }
    }

    /**
     * 新增联系我们的图片
     * @param guanyu
     * @return
     */
    @RequestMapping("/addGunayu2")
    public ResponseBean addGunayu2(@RequestBody Guanyu guanyu){
        List<Guanyu> list = guanyuService.list(new QueryWrapper<Guanyu>().eq("type",2));
        if (list.size()==0){
            if (guanyu!=null && guanyu.getPicture()!=null){
                String img = guanyu.getPicture();
                guanyu.setPicture("/images/goods/"+img);
                guanyu.setType(2);
                if (guanyuService.save(guanyu)){
                    FileUtil.copy(tmp + img, upload + "images/goods/" + img, true);
                    return new ResponseBean(200,"新增成功",guanyu);
                }else{
                    return new ResponseBean(500,"新增失败",null);
                }
            }else{
                return new ResponseBean(500,"图片为空",null);
            }
        }else {
            return new ResponseBean(500,"已有图片啊",null);
        }
    }

    /**
     * 修改图片
     * @param picture 图片路径
     * @return
     */
    @RequestMapping("/update")
    public ResponseBean update(String picture,String picture_en,String picture_th,String picture_kh){
        if (picture!=null && !"".equals(picture)){
            List<Guanyu> list = guanyuService.list(new QueryWrapper<Guanyu>().eq("type",1));
            if (list.size()>0){
                Guanyu guanyu = list.get(0);
                String img= null;
                String img2= null;
                String img3= null;
                String img4= null;
                if (!picture.startsWith("/images")){
                    img="/images/goods/"+picture;
                }else{
                    img=picture;
                }
                if (!picture_en.startsWith("/images")){
                    img2="/images/goods/"+picture_en;
                }else{
                    img2=picture_en;
                }
                if (!picture_th.startsWith("/images")){
                    img3="/images/goods/"+picture_th;
                }else{
                    img3=picture_th;
                }
                if (!picture_kh.startsWith("/images")){
                    img4="/images/goods/"+picture_kh;
                }else {
                    img4=picture_kh;
                }
                guanyu.setPicture(img);
                guanyu.setPicture_en(img2);
                guanyu.setPicture_th(img3);
                guanyu.setPicture_kh(img4);
                if (guanyuService.updateById(guanyu)){
                    if (!picture.startsWith("/images")){
                        FileUtil.copy(tmp + picture, upload + "images/goods/" + picture, true);
                    }
                    if (!picture_en.startsWith("/images")){
                        FileUtil.copy(tmp + picture_en, upload + "images/goods/" + picture_en, true);
                    }
                    if (!picture_th.startsWith("/images")){
                        FileUtil.copy(tmp + picture_th, upload + "images/goods/" + picture_th, true);
                    }
                    if (!picture_kh.startsWith("/images")){
                        FileUtil.copy(tmp + picture_kh, upload + "images/goods/" + picture_kh, true);
                    }
                    return new ResponseBean(200,"修改成功",guanyu);
                }else{
                    return new ResponseBean(500,"修改失败",null);
                }
            }else{
                return new ResponseBean(500,"请先新增图片",null);
            }
        }else{
            return new ResponseBean(500,"图片不能为空",null);
        }
    }

    /**
     * 修改图片
     * @param picture 图片路径
     * @return
     */
    @RequestMapping("/update2")
    public ResponseBean update2(String picture){
        if (picture!=null && !"".equals(picture)){
            List<Guanyu> list = guanyuService.list(new QueryWrapper<Guanyu>().eq("type",2));
            if (list.size()>0){
                Guanyu guanyu = list.get(0);
                String img= null;
                if (!picture.startsWith("/images")){
                    img="/images/goods/"+picture;
                }else{
                    img=picture;
                }
                guanyu.setPicture(img);
                if (guanyuService.updateById(guanyu)){
                    if (!picture.startsWith("/images")){
                        FileUtil.copy(tmp + picture, upload + "images/goods/" + picture, true);
                    }
                    return new ResponseBean(200,"修改成功",guanyu);
                }else{
                    return new ResponseBean(500,"修改失败",null);
                }
            }else{
                return new ResponseBean(500,"请先新增图片",null);
            }
        }else{
            return new ResponseBean(500,"图片不能为空",null);
        }
    }
}
