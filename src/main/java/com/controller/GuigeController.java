package com.controller;

import com.bean.ResponseBean;
import com.pojo.Guige;
import com.service.GuigeService;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author:TTW
 * @Discription:处理商品的规格
 */
@RestController
@RequestMapping("/guige")
public class GuigeController {
    @Autowired
    private GuigeService guigeService;

    /**
     * 根据用户查询出该用户的基本标签
     * @param uid
     * @return
     */
    @RequestMapping("guiges")
    public ResponseBean guiges(String uid,Integer type){
        if(uid == null) {
            return ResponseBean.error("用户不能为空") ;
        }
        List<Guige> list = guigeService.guiges(uid,type);
        return new ResponseBean(200 , "成功", list) ;
    }

    /**
     * 添加规格
     * @param uid 规格的uid值
     * @param name 规格名
     * @return
     */
    @RequestMapping("add_guige")
    public ResponseBean addGuige(String uid , String name,Integer type){
        if(StringUtil.isNull(name)) {
            return ResponseBean.error("规格名称不能为空") ;
        }
        if(uid == null) {
            return ResponseBean.error("请先登录") ;
        }
        String res = guigeService.addGuige(uid , name,type) ;
        return "成功".equals(res) ? ResponseBean.ok("成功") : ResponseBean.error(res) ;
    }

    /**
     * 当没有规格时添加默认的规格名：范围
     * @return
     */
    @RequestMapping("/addDefaultGuige")
    public ResponseBean addDefaultGuige(){
        List<Guige> guigeList=guigeService.list();
        if (guigeList.size()==0){
            Guige guige=new Guige();
            guige.setName("容量");
            guige.setUid("0");
            guige.setGtype(0);
            guigeService.save(guige);
        }
        return new ResponseBean(200,"ok",null);
    }
}
