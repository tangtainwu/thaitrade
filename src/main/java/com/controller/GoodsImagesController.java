package com.controller;

import com.bean.ResponseBean;
import com.service.GoodsImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * @Author:TTW
 * @Discription:处理商品的图片相关请求
 */
@RequestMapping("/goodsImages")
@RestController
public class GoodsImagesController {
    @Autowired
    private GoodsImagesService goodsImagesService;

    /**
     * 根据商品编号获取商品图片
     * @param gid
     * @return
     */
    @RequestMapping("/getGoodsImages")
    public ResponseBean getGoodsImages(String gid)
    {
        List goodsImages = goodsImagesService.getGoodsImages(gid);
        List goodsImagesEn = goodsImagesService.getGoodsImagesEn(gid);
        List goodsImagesTai = goodsImagesService.getGoodsImagesTai(gid);
        List goodsImagesKh = goodsImagesService.getGoodsImagesKh(gid);
        List list = Arrays.asList(goodsImages,goodsImagesEn,goodsImagesTai,goodsImagesKh);
        if (list==null || list.size()==0){
            return new ResponseBean(404,"没有找到",null);
        }
        return new ResponseBean(200,"获取成功",list);
    }
}
