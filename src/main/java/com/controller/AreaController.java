package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bean.ResponseBean;
import com.dao.AreaDAO;
import com.pojo.Advertise;
import com.pojo.Area;
import com.service.AreaService;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * 区域管理
 * @Author:ttw
 * @Date:2022-02-23
 */
@RestController
@RequestMapping("/area")
public class AreaController {
    @Autowired
    private AreaService areaService;

    /**
     * 分页查询区域
     * @param page 页数
     * @param limit 条数
     * @param areaName 区域名称
     * @return
     */
    @RequestMapping("/selectArea")
    public ResponseBean selectArea(int page, int limit, @RequestParam(value = "areaName",defaultValue = "") String areaName){
        IPage iPage = areaService.getPage(page,limit,areaName);
        return new ResponseBean(0,iPage.getTotal(),"成功",iPage.getRecords());
    }

    /**
     * 添加区域
     * @param area
     * @return
     */
    @RequestMapping("/addArea")
    public ResponseBean addArea(@RequestBody Area area){
        if (StringUtil.isNull(area.getAreaName_en()) || StringUtil.isNull(area.getAreaName_tai()) || StringUtil.isNull(area.getAreaName()) || StringUtil.isNull(area.getAreaDescribe()) || StringUtil.isNull(area.getAreaName_kh())){
            return new ResponseBean(500,"必填项不能为空",null);
        }
        area.setCreateTime(new Date());
        if (areaService.save(area)){
            return new ResponseBean(200,"添加成功",area);
        }else{
            return new ResponseBean(500,"添加失败",null);
        }
    }

    /**
     * 单元格编辑
     * @param id id
     * @param value 修改的值
     * @param columname 修改的字段
     * @return
     */
    @RequestMapping("/updateArea")
    public ResponseBean updateArea(String id,String value ,String columname){
        int n=areaService.updateArea(id,value,columname);
        if (n>0){
            return new ResponseBean(0,1,"修改成功","");
        }else{
            return new ResponseBean(1,1,"修改失败","");
        }
    }

    /**
     * 根据id删除
     * @param id id
     * @return
     */
    @RequestMapping("/delete")
    public ResponseBean delete(String id){
        Area area = areaService.getById(id);
        if (area!=null){
            if (areaService.removeById(area.getId())){
                return new ResponseBean(200,"删除成功",null);
            }else{
                return new ResponseBean(500,"删除失败",area);
            }
        }else{
            return new ResponseBean(500,"区域不存在",id);
        }
    }

    /**
     * 获取所有区域
     * @return
     */
    @RequestMapping("/getAllArea")
    public ResponseBean getAllArea(String yuyan){
        List<Area> list = areaService.getList(yuyan);
        System.out.println(list);
        return new ResponseBean(200,"成功",list);
    }
}
