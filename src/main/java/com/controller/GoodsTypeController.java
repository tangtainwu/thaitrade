package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bean.ResponseBean;
import com.pojo.GoodsType;
import com.pojo.TypeData;
import com.service.GoodsTypeService;
import com.sun.org.apache.xpath.internal.objects.XNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author: ttw
 * @Discription:处理商品类型的请求
 */
@RestController
@RequestMapping("/admin/GoodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 新增商品类型
     * @param s 商品类型的对象
     * @return
     */
    @RequestMapping("/insert")
    public ResponseBean insert(String s) {
        int i = goodsTypeService.insert(s);
        if (i==-1){
            return new ResponseBean(500,"商品类型名称重复",i);
        }
        return new ResponseBean(i > 0 ? 200 : 500, i > 0 ? "新增成功" : "新增失败", i);
    }

    /**
     * 修改商品类型
     * @param s 商品类型对象
     * @param id 商品类型id
     * @return
     */
    @RequestMapping("/update")
    public ResponseBean update(String s, String id) {
        int i = goodsTypeService.update(s, id);
        if (i==-1){
            return new ResponseBean(500,"类别不能超过三级",i);
        }else if (i==-2){
            return new ResponseBean(500,"修改的上级类别不能为自己的子类别",i);
        }
        return new ResponseBean(i > 0 ? 200 : 500, i > 0 ? "修改成功" : "修改失败", i);
    }

    /**
     * 所有类型分页
     *
     * @param page 页数
     * @param limit 每页的条数
     * @param gname 根据名字查询
     * @return
     */
    @RequestMapping("/fenye")
    public ResponseBean selectFenye(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit, String gname) {
        IPage<GoodsType> i = goodsTypeService.selectFenye(page, limit, gname);
        return new ResponseBean(0, i.getTotal(), i.getRecords());
    }

    /**
     * 删除
     * @param id 商品类型id
     * @return
     */
    @RequestMapping("/delete")
    public ResponseBean delete(String id) {
        String i = goodsTypeService.delete(id);
        return new ResponseBean(i == null ? 200 : 500, i == null ? "删除成功" : "删除失败", i);
    }

    /**
     * 查询所有类型
     * x为2时代表所有选项可以点击
     * @return
     */
    @RequestMapping("/select")
    public ResponseBean select(Integer x) {
        TypeData l = goodsTypeService.select(x);
        return new ResponseBean(200, "查询成功", l);
    }

    /**
     * 根据id查商品类型
     * @param id 查询的id
     * @return
     */
    @RequestMapping("/selectById")
    public ResponseBean selectById(String id) {
        GoodsType l = goodsTypeService.selectById(id);
        return new ResponseBean(l != null ? 200 : 500, l != null ? "查询成功" : "查询失败", l);
    }

    /**
     * 根据id查它的上级上级的上级
     * @param id
     * @return
     */
    @RequestMapping("/selectParentById")
    public ResponseBean selectParentById(String id) {
        List<GoodsType> l = goodsTypeService.selectParentById(id);
        if (l == null) {
            return new ResponseBean(200, "查询成功", "/类型列表");
        }
        String s = goodsTypeService.selectParentName(l);
        return new ResponseBean(200, "查询成功", s);
    }

    /**
     * 客户端获取一,二，三级类别
     * @param grade 查询的类别等级，1,2,3
     * @return
     */
    @CrossOrigin
    @RequestMapping("/getGoodsTypeOne")
    public ResponseBean getGoodsTypeOne(@RequestParam(defaultValue = "1") int grade){
        List<GoodsType> list=goodsTypeService.list(new QueryWrapper<GoodsType>().eq("grade",grade));
        if (list.size()>0){
            return new ResponseBean(200,"查询成功",list);
        }else{
            return new ResponseBean(500,"没有查到该类别",list);
        }
    }

    /**
     * 根据父级类别的id查询下级类别
     * @param parentId 父级类别的id
     * @return
     */
    @RequestMapping("/getGoodsTypeByParentId")
    public ResponseBean getGoodsTypeByParentId(String parentId){
        List<GoodsType> list=goodsTypeService.list(new QueryWrapper<GoodsType>().eq("superior_id",parentId));
        return new ResponseBean(200,"查询成功",list);
    }

    /**
     * 查询所有一级类别，并且得到所属的二级，三级类别
     * @param yuyan 语言
     * @return
     */
    @CrossOrigin
    @RequestMapping("/getGoodsFenlei")
    public ResponseBean getGoodsFenlei(String yuyan){
        List<GoodsType> goodsTypes = goodsTypeService.getGoodsType(yuyan);
        return new ResponseBean(200,goodsTypes.size(),"查询成功",goodsTypes);
    }

}
