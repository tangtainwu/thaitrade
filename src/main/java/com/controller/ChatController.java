package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bean.ResponseBean;
import com.pojo.Chat;
import com.pojo.Chatdetail;
import com.pojo.Users;
import com.service.ChatService;
import com.service.ChatdetailService;
import com.service.UsersService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author:TTW
 * @Discription:聊天记录信息处理
 */
@RestController
@RequestMapping("/chat")
public class ChatController {
    @Autowired
    private ChatService chatService;
    @Autowired
    private ChatdetailService chatdetailService;
    @Autowired
    private UsersService usersService;

    /**
     * 根据客服名和用户名加载历史聊天记录（客服用）
     *
     * @param kefuname 客服名
     * @param username 用户名
     * @return
     */
    @RequestMapping("/getHistoryChat")
    public ResponseBean getHistoryChat(String kefuname, String username) {
        List<Chatdetail> chatdetailList = chatService.getHistoryChat(kefuname, username);
        if (chatdetailList.size() > 0) {
            return new ResponseBean(200, "加载历史记录", chatdetailList);
        } else {
            return new ResponseBean(500, "没有历史记录", chatdetailList);
        }
    }

    /**
     * 根据用户名获取该用户所有的历史记录
     * @param username 用户名
     * @return
     */
    @RequestMapping("/getHistoryUser")
    public ResponseBean getHistoryUser(String username){
        List<Chatdetail> chatdetailList=chatService.getHistoryUser(username);
        if (chatdetailList.size() > 0) {
            return new ResponseBean(200, "加载历史记录", chatdetailList);
        } else {
            return new ResponseBean(500, "没有历史记录", chatdetailList);
        }
    }

    /**
     * 根据客服名，查询该客服所有的聊天用户
     * @param kefuname 客服名
     * @param page 页数
     * @param limit 条数
     * @return
     */
    @RequestMapping("/getChatForUser")
    public ResponseBean getChatForUser(@RequestParam(defaultValue = "1") int page,@RequestParam(defaultValue = "10") int limit, String kefuname){
        IPage iPage =chatService.getChatForUser(kefuname,page,limit);
        return new ResponseBean(0,iPage.getTotal(),"查询成功",iPage.getRecords());
    }

    /**
     * 修改未读记录为1
     * @param kefuname 客服名
     * @param username 用户名
     * @return
     */
    @RequestMapping("/editWeidu")
    public ResponseBean editWeidu(String kefuname,String username){
        Chat chat=chatService.getOne(new QueryWrapper<Chat>().eq("kefuName",kefuname).eq("userName",username));
        if (chat!=null){
            UpdateWrapper<Chatdetail> updateWrapper=new UpdateWrapper<>();
            updateWrapper.eq("chatId",chat.getId());
            updateWrapper.set("status",1);
            if(chatdetailService.update(updateWrapper)){
                return ResponseBean.ok("成功改为已读");
            }else{
                return ResponseBean.error("修改失败");
            }
        }else{
            return ResponseBean.error("未找到");
        }
    }

    /**
     * 加载客服与这位用户的未读消息
     * @param kefuname 客服名
     * @param username 用户名
     * @return
     */
    @RequestMapping("/addnewChat")
    public ResponseBean addnewChat(String kefuname,String username){
        List<Chatdetail> chatdetailList=chatService.addnewChat(kefuname,username);
        if (chatdetailList.size() > 0) {
            return new ResponseBean(200, "加载新记录", chatdetailList);
        } else {
            return new ResponseBean(500, "没有新记录", chatdetailList);
        }
    }

    /**
     * 用户打开聊天，找到客服，发送我是客服。。。
     * @param kefuname 客服名
     * @param username 用户名
     * @return
     */
    @RequestMapping("/iAmKefu")
    public ResponseBean iAmKefu(String kefuname,String username){
        String ss = chatService.iAmKefu(kefuname,username);
        return ResponseBean.ok(ss);
    }

    /**
     * 获取用户未读的消息个数
     * @param username 用户名
     * @return
     */
    @RequestMapping("/getUserHintNumber")
    public ResponseBean getUserHintNumber(String username){
        int n = chatService.getUserHintNumber(username);
        return new ResponseBean(200,"查询成功",n);
    }

    /**
     * 将用户的未读消息改为已读
     * @param username 用户名
     * @return
     */
    @RequestMapping("/useHaveReadMessage")
    public ResponseBean useHaveReadMessage(String username){
        Users user = usersService.getOne(new QueryWrapper<Users>().eq("utel",username).last("limit 1"));
        if (user==null){
            return ResponseBean.error("未找到用户");
        }
        UpdateWrapper<Chatdetail> updateWrapper=new UpdateWrapper<>();
        updateWrapper.eq("uname",user.getUname());
        updateWrapper.set("useStatus",1);
        if(chatdetailService.update(updateWrapper)){
            return ResponseBean.ok("成功改为已读");
        }else{
            return ResponseBean.error("修改失败");
        }
    }

}
