package com.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import com.bean.ResponseBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * @Author:TTW
 * @Discription:商城规格的图片
 */
@Controller
@RequestMapping("/upload.do")
public class UploadGuigeAction {

    @Value("${upload.dir}")
    private String upload;

    @Value("${upload.tmp.dir}")
    private String tmp ;

    /**
     * 上传到临时目录
     * @param response
     * @return
     * @throws IllegalStateException
     * @throws IOException
     */
    @RequestMapping("/upload")
    @ResponseBody
    public String upload(MultipartHttpServletRequest mrequest, HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException
    {

        String path = tmp ;
        File pathFile=new File(path);
        if(pathFile.exists()==false)//如果不存在的话
            pathFile.mkdir();//就新建一个


        MultipartFile file = null;
        Iterator<String> it = mrequest.getFileNames();
        if (it.hasNext()==false)
        {
            return null;
        }
        String name = it.next();
        file = mrequest.getFile(name);
        String oldName = file.getOriginalFilename();//旧的文件名
        String newName = newName(oldName); //新的文件
        file.transferTo(new File(path+"/"+newName)); //上传到临时目录
        return newName;
    }

    @RequestMapping(params = "p=layui")
    @ResponseBody
    public ResponseBean uploadLayui(MultipartHttpServletRequest mrequest, HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException{
        String fileName = upload(mrequest , request , response) ;
        return new ResponseBean(0 , "success" , fileName) ;
    }

    /**
     * 上传到临时目录
     * @param response
     * @return
     * @throws IllegalStateException
     * @throws IOException
     */
    @RequestMapping(params="p=msg")
    @ResponseBody
    public String msgFileUpload(MultipartHttpServletRequest mrequest,HttpServletRequest request,HttpServletResponse response) throws IllegalStateException, IOException
    {
        String path = upload+"images/msg";
//        System.out.println(path);
        File pathFile=new File(path);
        if(pathFile.exists()==false)//如果不存在的话
            pathFile.mkdir();//就新建一个


        MultipartFile  file = null;
        Iterator<String> it = mrequest.getFileNames();
        if (it.hasNext()==false)
        {
            return null;
        }
        String name = it.next();
        file = mrequest.getFile(name);
        String oldName = file.getOriginalFilename();//旧的文件名
        String newName = newName(oldName); //新的文件
        file.transferTo(new File(path+"/"+newName)); //上传到临时目录
        return newName;
    }

    /**
     * 上传到临时目录
     * @return
     * @throws IllegalStateException
     * @throws IOException
     */
    @RequestMapping(params="p=msg1")
    @ResponseBody
    public String msg1FileUpload(String fileName , String fileContent , HttpServletRequest request) throws IllegalStateException, IOException
    {
        String path = upload+"images/msg";
//        System.out.println(path);
        File pathFile=new File(path);
        if(pathFile.exists()==false)//如果不存在的话
            pathFile.mkdir();//就新建一个


        String oldName = fileName;//旧的文件名
        String newName = newName(oldName); //新的文件
        byte[] data = Base64.decode(fileContent) ;
        FileUtil.writeBytes(data, path + "/" + newName) ;
        return newName;
    }


    //产生新的文件名 a.png
    private String newName(String oldName)
    {
        int index = oldName.lastIndexOf(".");
        String extName = oldName.substring(index);//得后辍名
        if(".apk".equals(extName))
            return "携联助手"+extName;
        return System.currentTimeMillis()+extName;
    }
}
