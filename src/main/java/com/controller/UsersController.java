package com.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bean.ResponseBean;
import com.pojo.Kefu;
import com.pojo.Users;
import com.service.KefuService;
import com.service.UsersService;
import com.util.SmsUtil;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Liao
 * @Date: 2021-10-19
 * @Description: 用户相关接口处理
 */
@RestController
@RequestMapping("/users")
public class UsersController {
    @Autowired
    private UsersService usersService;
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 用户密码登录
     * @param utel 用户手机
     * @param upsw 密码
     * @return
     */
    @RequestMapping("/login")
    @CrossOrigin
    public ResponseBean usersLogin(String utel,String upsw, HttpServletRequest request){
        if(utel.length()<1 || upsw.length()<1){
            return  ResponseBean.error("手机号或密码不能为空");
        }
        Users users = usersService.getOne(new QueryWrapper<Users>().eq("utel",utel).eq("upsw",upsw));
        if (users!=null){
            if(users.getUstatus()==0){
                return ResponseBean.error("用户已禁用");
            }else{
                request.getSession().setAttribute("users" ,users);
                return new ResponseBean(200,"登录成功",users);
            }
        }else{
            return ResponseBean.error("用户名或密码错误");
        }
    }
    /**
     * 生成验证码
     * @param utel 用户手机
     * @return
     */
    @RequestMapping("/setcode")
    @CrossOrigin
    public ResponseBean setCode(String utel,HttpServletRequest request){
        if(utel.length()<1){
            return  ResponseBean.error("手机号不能为空");
        }
        StringBuffer sb = new StringBuffer();
        System.out.println(utel);
        Random r = new Random();
        for(int i=0;i<4;i++){
            int n = r.nextInt(10);
           sb.append(n);
        }
        System.out.println(sb.toString() );
        // 验证码存入Redis
        redisTemplate.boundValueOps(utel).set(sb.toString(),5, TimeUnit.MINUTES);//5分钟有效
        // 发送短信验证码
       boolean f =  SmsUtil.send(utel,sb.toString());
//        boolean f = true;
       if(f)
        return  ResponseBean.ok("发送成功，请注意查收");
       else
           return  ResponseBean.error("发送失败");
    }
    /**
     * 用户短信登录
     * @param utel 用户手机
     * @param code 验证码
     * @return
     */
    @RequestMapping("/loginByCode")
    @CrossOrigin
    public ResponseBean loginByCode(String utel,String code, HttpServletRequest request){
        String redisPhone = (String) redisTemplate.boundValueOps(utel).get();   // 缓存中的验证码
        Users users = usersService.getOne(new QueryWrapper<Users>().eq("utel",utel));
        if(redisPhone==null || redisPhone.equals(code)==false){
            return  ResponseBean.error("验证码错误");
        }
        if (users!=null){
            if(users.getUstatus()==0){
                return ResponseBean.error("用户已禁用");
            }else{
                redisTemplate.boundValueOps(utel).expire(0, TimeUnit.MILLISECONDS); // 清空验证码
                request.getSession().setAttribute("users" ,users);
                return new ResponseBean(200,"登录成功",users);

            }
        }else{
            return ResponseBean.error("用户名不存在");
        }
    }

    /**
     * 用户注册
     * @param utel 用户名字
     * @param upsw 密码
     * @return
     */
    @RequestMapping("/regist")
    @CrossOrigin
    public ResponseBean usersRegist(String utel,String upsw,String uname,String code,String uaddress, HttpServletRequest request){
        if(utel.length()<1 || upsw.length()<1){
            return  ResponseBean.error("手机号或密码不能为空");
        }
        if(uname.length()<1)
        {
            return  ResponseBean.error("用户名不能为空");
        }
        if(uaddress.length()<1){
            return  ResponseBean.error("用户地址不能为空");
        }
        String redisPhone = (String) redisTemplate.boundValueOps(utel).get();   // 缓存中的验证码
        if(redisPhone==null || redisPhone.equals(code)==false){
            return  ResponseBean.error("验证码错误");
        }
        Users oldusers = usersService.getOne(new QueryWrapper<Users>().eq("utel",utel));    // 查找是否存在该手机号码
        if(oldusers!=null){
            return ResponseBean.error("手机号已存在");
        }
        Users users = new Users();
        users.setUaddress(uaddress);
        users.setUname(uname);
        users.setUpsw(upsw);
        users.setUtel(utel);
        users.setType(1);
        users.setCreateTime(new Date());
        boolean f = usersService.save(users);
        if(f){
            return ResponseBean.ok("注册成功");
        }else{
            return ResponseBean.error("注册失败");
        }
    }

    /**
     * 分页查询用户列表
     * @param page 页数
     * @param limit 条数
     * @param uname 搜索 用户的名字
     * @param type 用户类型
     * @return
     */
    @RequestMapping("/selectUsers")
    public ResponseBean selectUsers(int page,int limit,String uname,@RequestParam(defaultValue = "-1") int type){
        IPage iPage = usersService.selectPage(page,limit,uname,type);
        return new ResponseBean(0,iPage.getTotal(),"查询成功",iPage.getRecords());
    }



}
