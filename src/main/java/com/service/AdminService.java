package com.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.AdminDAO;
import com.pojo.Admin;
import org.springframework.stereotype.Service;

/**
 * @Author:ttw
 * @Discription:后台用户账号管理
 */
@Service
public class AdminService extends ServiceImpl<AdminDAO, Admin> {

}
