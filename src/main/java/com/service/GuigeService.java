package com.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.BaseguigeDAO;
import com.dao.GuigeDAO;
import com.pojo.Baseguige;
import com.pojo.Guige;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: ttw
 * @Description: 处理规格的service层
 */
@Service
public class GuigeService extends ServiceImpl<GuigeDAO, Guige> {
    @Autowired
    private GuigeDAO guigeDAO;
    @Autowired
    private BaseguigeDAO baseguigeDAO;

    /**
     * 根据uid查询规格
     * @param uid
     * @return
     */
    public List<Guige> guiges(String uid,int gtype){
        List<Guige> guiges = guigeDAO.selectList(new QueryWrapper<Guige>().eq("uid" , uid).eq("gtype",gtype)) ;
        List<Baseguige> baseguiges = baseguigeDAO.selectList(null) ;
        if(guiges == null) {
            guiges = new ArrayList<>() ;
        }
        for(Baseguige a : baseguiges) {
            Guige guige = new Guige() ;
            BeanUtil.copyProperties(a , guige);
            guiges.add(guige);
        }
        return guiges ;
    }

    /**
     * 根据uid和规格名新增规格
     * @param uid
     * @param name
     * @return
     */
    public String addGuige(String uid , String name,int gtype){
        Guige g = guigeDAO.selectOne(new QueryWrapper<Guige>().eq("name" , name).eq("uid" , uid).eq("gtype",gtype)) ;
        if(g != null) {
            return "规格已经存在，不能重复添加" ;
        }
        Guige guige = new Guige() ;
        guige.setUid(uid);
        guige.setName(name);
        guige.setGtype(gtype);
        guigeDAO.insert(guige) ;
        return "成功" ;
    }
}
