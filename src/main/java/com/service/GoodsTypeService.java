package com.service;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.GoodsDAO;
import com.dao.GoodsTypeDAO;
import com.pojo.Goods;
import com.pojo.GoodsType;
import com.pojo.TypeData;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ttw
 * @Discription:商品类型管理的servic层
 */
@SuppressWarnings("ALL")
@Service
public class GoodsTypeService extends ServiceImpl<GoodsTypeDAO, GoodsType> {

    @Autowired
    private GoodsTypeDAO goodsTypeDAO;
    @Autowired
    private GoodsDAO goodsDAO;
    /**
     * 文件上传的目录
     */
    @Value("${upload.dir}")
    private String upload_dir;

    /**
     * 上传临时目录
     */
    @Value("${upload.tmp.dir}")
    private String upload_tmp_dir;

    /**
     * @param yuyan 语言
     * @return
     * @Author:ttw 查询所有一级类别，并且得到所属的二级，三级类别
     */
    public List<GoodsType> getGoodsType(String yuyan) {
        List<GoodsType> goodsTypeList = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("grade", 1));
        for (GoodsType goodsType:goodsTypeList) {
            List<GoodsType> goodsTypeList2=goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id",goodsType.getId()).eq("grade",2));
            for (GoodsType goodsType2:goodsTypeList2) {
                List<GoodsType> goodsTypeList3=goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id",goodsType2.getId()).eq("grade",3));
                goodsType2.setChildren(goodsTypeList3);
            }
            goodsType.setChildren(goodsTypeList2);
        }
//        for (int i = 0; i < goodsTypeList.size(); i++) {
//            List<GoodsType> goodsTypeList2 = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id", goodsTypeList.get(i).getId()).eq("grade", 2));
//            for (int j = 0; j < goodsTypeList2.size(); j++) {
//                List<GoodsType> goodsTypeList3 = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id", goodsTypeList2.get(j).getId()).eq("grade", 3));
//                for (int k=0;k<goodsTypeList3.size();k++){
//                    List<Goods> goodsList=goodsDAO.selectList(new QueryWrapper<Goods>().eq("gTypeId",goodsTypeList3.get(k).getId()).eq("gStatus",1));
//                    if (goodsList.size()>0){
//                        goodsTypeList2.get(j).setChildren(goodsTypeList3);
//                        break;
//                    }
//                }
//            }
//            goodsTypeList.get(i).setChildren(goodsTypeList2);
//        }
        if ("zh".equals(yuyan)) {
            for (GoodsType goodsType : goodsTypeList) {
                if (StringUtil.isNull(goodsType.getGname())) {
                    goodsType.setGname(goodsType.getGname_tai());
                }
                for (GoodsType goodsType2 : goodsType.getChildren()) {
                    if (StringUtil.isNull(goodsType2.getGname())) {
                        goodsType2.setGname(goodsType2.getGname_tai());
                    }
                    for (GoodsType goodsType3 : goodsType2.getChildren()) {
                        if (StringUtil.isNull(goodsType3.getGname())) {
                            goodsType3.setGname(goodsType3.getGname_tai());
                        }
                    }
                }
            }
        } else if ("en".equals(yuyan)) {
            for (GoodsType goodsType : goodsTypeList) {
                if (StringUtil.isNull(goodsType.getGname_en())) {
                    goodsType.setGname(goodsType.getGname_tai());
                } else {
                    goodsType.setGname(goodsType.getGname_en());
                }
                for (GoodsType goodsType2 : goodsType.getChildren()) {
                    if (StringUtil.isNull(goodsType2.getGname_en())) {
                        goodsType2.setGname(goodsType2.getGname_tai());
                    } else {
                        goodsType2.setGname(goodsType2.getGname_en());
                    }
                    for (GoodsType goodsType3 : goodsType2.getChildren()) {
                        if (StringUtil.isNull(goodsType3.getGname_en())) {
                            goodsType3.setGname(goodsType3.getGname_tai());
                        } else {
                            goodsType3.setGname(goodsType3.getGname_en());
                        }
                    }
                }
            }
        } else if ("th".equals(yuyan)) {
            for (GoodsType goodsType : goodsTypeList) {
                goodsType.setGname(goodsType.getGname_tai());
                for (GoodsType goodsType2 : goodsType.getChildren()) {
                    goodsType2.setGname(goodsType2.getGname_tai());
                    for (GoodsType goodsType3 : goodsType2.getChildren()) {
                        goodsType3.setGname(goodsType3.getGname_tai());
                    }
                }
            }
        }
        return goodsTypeList;
    }

    /**
     * 首页查询四个类别（按商品销量）
     *
     * @return
     */
    public List<Goods> searchSigeYijiType() {
        return goodsTypeDAO.searchSigeYijiType();
    }

    /**
     * 新增商品类型
     *
     * @param s 商品类型对象
     * @return
     */
    public int insert(String s) {
        GoodsType goodsType = JSON.parseObject(s, GoodsType.class);
        List list = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("gname", goodsType.getGname()).eq("superior_id", goodsType.getSuperior_id()));
        if (list.size() > 0) {
            return -1;
        }
        String url = goodsType.getIcon();
        goodsType.setCreateTime(new Date());
        goodsType.setIcon("upload/goodsType/" + goodsType.getIcon());
        new Thread() {
            @Override
            public void run() {
                FileUtil.copy(upload_tmp_dir + url, upload_dir + "goodsType/", true);
            }
        }.start();
        return goodsTypeDAO.insert(goodsType);
    }


    /**
     * 查询所有的类型（树形显示）
     *
     * @param x x为2时：表示新增商品时查询所有类型，只有三级类别可以点击
     * @return
     */
    public TypeData select(int x) {
        TypeData root = new TypeData("0", "类型列表", null, false, false, true, new ArrayList<TypeData>(), null, 0);
        List<GoodsType> none_parent_types = goodsTypeDAO.selectNoneparent();
        if (none_parent_types == null) {
            return root;
        }

        List<GoodsType> parents = goodsTypeDAO.selectparent();
        if (parents == null) {
            List<TypeData> none_prent = TypeData.toTypeData(none_parent_types, x);
            for (TypeData n : none_prent) {
                root.addChildNode(n);
            }
            return root;
        }

        List<String> parent_ids = goodsTypeDAO.selectparent_ID();

        List<List<GoodsType>> xiajiLists = new ArrayList<>();
        for (String i : parent_ids) {
            QueryWrapper q = new QueryWrapper();
            q.eq("superior_id", i);
            GoodsType g = goodsTypeDAO.selectById(i);
            if (g == null) {
                continue;
            }
            List<GoodsType> ts = goodsTypeDAO.selectList(q);
//            if (ts == null) {
//                continue;
//            }
            xiajiLists.add(ts);
        }
        List<TypeData> none_prent = TypeData.toTypeData(none_parent_types, x);
        List<TypeData> t_prents = TypeData.toTypeData(parents, x);

        List<List<TypeData>> typeDataLists = new ArrayList<>();
        for (List<GoodsType> l : xiajiLists) {
            List<TypeData> t_xiajis = TypeData.toTypeData(l, x);
            typeDataLists.add(t_xiajis);
        }
        int len = typeDataLists.size();
        for (int i = 0; i < len; i++) {
            if (t_prents.get(i) == null) {
                continue;
            }
            t_prents.get(i).setChildren(typeDataLists.get(i));
        }

        for (TypeData t : t_prents) {
            List<TypeData> temp = t.getChildren();
            for (TypeData t2 : t_prents) {
                if (t2.getParent_ID() == null) {
                    continue;
                }
                if (t.getId().equals(t2.getParent_ID())) {
                    for (TypeData td : temp) {
                        if (td.getId().equals(t2.getId())) {
                            //删除相同的
                            temp.remove(td);
                            break;
                        }
                    }
                    temp.add(t2);
                }
            }
            t.setChildren(temp);
        }
        for (TypeData n : none_prent) {
            boolean b = true;
            for (TypeData t : t_prents) {
                if (n.getId().equals(t.getId())) {
                    root.addChildNode(t);
                    b = false;
                    break;
                }
            }
            if (b) {
                root.addChildNode(n);
            }
        }
        return root;
    }

    /**
     * 分页查询商品类型
     *
     * @param page  页数
     * @param limit 每页条数
     * @param gname 根据名字查询
     * @return
     */
    public IPage selectFenye(Integer page, Integer limit, String gname) {
        QueryWrapper<GoodsType> q = new QueryWrapper();
        q.orderByDesc("createTime");
        if (StringUtil.isNotNull(gname)) {
            q.like("gname", gname);
        }
        IPage iPage = goodsTypeDAO.selectPage(new Page<>(page, limit), q);
        List<GoodsType> list = iPage.getRecords();
        for (int i = 0; i < list.size(); i++) {
            GoodsType parent = goodsTypeDAO.selectById(list.get(i).getSuperior_id());
            list.get(i).setParent(parent);
        }
        iPage.setRecords(list);
        return iPage;
    }

    /**
     * 删除商品类型
     * 如果删除的类别下有子类别，将子类别也一并删除，判断这些类别是否有上架商品
     *
     * @param id
     * @return
     */
    public String delete(String id) {
        if (id == null) {
            return "删除失败";
        }
        List<GoodsType> goodsTypeList = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id",id));
        if (goodsTypeList.size()>0){
            return "该类别下有子类别，不能删除";
        }
        List<Goods> goodsList = goodsDAO.selectList(new QueryWrapper<Goods>().eq("gTypeId", id));
        if (goodsList.size() > 0) {
            return "该类别下有商品，不能删除";
        }
//        //先判断该类别和子类别是否可删除
//        GoodsType goodsType=goodsTypeDAO.selectById(id);
//        int n = judgeChilrenWhtherDelete(goodsType);//n>0则可以删除
        if (goodsTypeDAO.deleteById(id)>0) {
            return null;
        } else {
            return "类别删除失败";
        }
    }

    /**
     * 判断该类别和子类别是否可删除
     * @param id 要删除的类别id
     */
    private int judgeChilrenWhtherDelete(GoodsType goodsType){
        if (goodsType.getGrade()==1 ||goodsType.getGrade()==2){
            List<GoodsType> list = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id",goodsType.getId()));
            int n = 0;
            for (GoodsType type :list) {
                n += judgeChilrenWhtherDelete(type);
            }
            return n;
        }else{
            List<Goods> goodsList = goodsDAO.selectList(new QueryWrapper<Goods>().eq("gTypeId", goodsType.getId()));
            return goodsList.size();
        }
    }

    /**
     * 删除子类别
     *
     * @param id 类别id
     * @return
     */
    private String deleteChildrenType(String id) {
        List<GoodsType> goodsTypeList = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().in("grade", 2, 3).eq("superior_id", id));
        for (GoodsType goodstype : goodsTypeList) {
            List<Goods> list = goodsDAO.selectList(new QueryWrapper<Goods>().eq("gTypeId", goodstype.getId()).eq("gStatus", 1));
            if (list.size() > 0) {
                return "子类别还有上架商品，不能删除该类别";
            }
            int m = goodsTypeDAO.deleteById(goodstype.getId());
            if (m <= 0) {
                return "子类别删除失败";
            } else {
                return deleteChildrenType(goodstype.getId());
            }
        }
        return null;
    }

    /**
     * 修改商品类型
     *
     * @param s  商品类型对象
     * @param id 商品类型id
     * @return
     */
    public int update(String s, String id) {
        if (s == null || id == null) {
            return 0;
        }
        GoodsType gt = goodsTypeDAO.selectById(id);

        if (gt == null) {
            return 0;
        }

        GoodsType gt2 = JSON.parseObject(s, GoodsType.class);
        if (gt2.getSuperior_id()!=null && "".equals(gt2.getSuperior_id())) {
            gt2.setSuperior_id(null);
        }
        jiema(gt2);//解码商品分类的名称
        //判断修改后的父类别是不是自己的子类别
        if (judgeChilren(gt2.getSuperior_id(), gt.getId())) {
            return -2;
        }
        GoodsType parentType = goodsTypeDAO.selectById(gt2.getSuperior_id());
        int parentTypeGrade = 0;
        if (parentType != null) {
            parentTypeGrade = parentType.getGrade();
        }
        //判断修改后类别等级是否超过3级
        int ss = judgeGrade(parentTypeGrade + 1, gt);
        if (ss > 3) {
            return -1;
        }
        String url = gt2.getIcon();
        if (gt2.getGname() != null && gt2.getGname().trim().length() != 0) {
            gt.setGname(gt2.getGname());
        }
        if (gt2.getGname_en() != null && gt2.getGname_en().trim().length() != 0) {
            gt.setGname_en(gt2.getGname_en());
        }
        if (gt2.getGname_tai() != null && gt2.getGname_tai().trim().length() != 0) {
            gt.setGname_tai(gt2.getGname_tai());
        }
        if (gt2.getGname_kh() != null && gt2.getGname_kh().trim().length() != 0) {
            gt.setGname_kh(gt2.getGname_kh());
        }
        gt.setSuperior_id(gt2.getSuperior_id());
        gt.setGrade(parentTypeGrade + 1);
        if (url != null && !url.equals(gt.getIcon())) {
            if (gt2.getIcon() != null) {
                gt.setIcon("upload/goodsType/" + gt2.getIcon());
            }
            new Thread() {
                @Override
                public void run() {
                    FileUtil.copy(upload_tmp_dir + url, upload_dir + "goodsType/", true);
                }
            }.start();
        }

        int count = goodsTypeDAO.updateById(gt);
        if (count > 0) {
            //修改子类别的grade属性
            List<GoodsType> goodsTypeList = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id", gt.getId()));
            for (GoodsType goodstype : goodsTypeList) {
                goodstype.setGrade(gt.getGrade() + 1);
                goodsTypeDAO.updateById(goodstype);
            }
        }
        return count;
    }

    /**
     * 判断修改后的父类别是不是自己的子类别
     *
     * @param parentId 修改后的父类别id
     * @param id       修改的类别本身id
     * @return
     */
    private Boolean judgeChilren(String parentId, String id) {
        List<GoodsType> list = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id", id));
        for (GoodsType goodstype : list) {
            if (goodstype.getId() == parentId) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断修改后类别等级是否超过3级
     *
     * @param grade     该类型现有的等级
     * @param goodsType 商品类型对象
     * @return
     */
    private int judgeGrade(int grade, GoodsType goodsType) {
        List<GoodsType> goodsTypeList = goodsTypeDAO.selectList(new QueryWrapper<GoodsType>().eq("superior_id", goodsType.getId()));
        if (goodsTypeList.size() > 0) {
            grade++;
            for (GoodsType goodsType1 : goodsTypeList) {
                int n = judgeGrade(grade, goodsType1);
                if (n != grade) {
                    grade++;
                    break;
                }
            }
        }
        return grade;
    }

    /**
     * 解码
     *
     * @param uKefu
     */
    private void jiema(GoodsType g) {
        try {
            g.setGname(URLDecoder.decode(g.getGname(), "utf-8"));
            g.setGname_en(URLDecoder.decode(g.getGname_en(), "utf-8"));
            g.setGname_tai(URLDecoder.decode(g.getGname_tai(), "utf-8"));
            g.setIcon(URLDecoder.decode(g.getIcon(), "utf-8"));
            g.setGname_kh(URLDecoder.decode(g.getGname_kh(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据id查
     *
     * @param id
     * @return
     */
    public GoodsType selectById(String id) {
        if (id == null) {
            return null;
        }
        return goodsTypeDAO.selectById(id);
    }

    /**
     * 查询父类别的名字
     *
     * @param s 商品类型
     * @return
     */
    public String selectParentName(List<GoodsType> s) {
        StringBuffer sb = new StringBuffer("类型列表");
        for (int i = s.size() - 1; i > -1; i--) {
            sb.append("/").append(s.get(i).getGname());
        }
        return sb.toString();
    }

    /**
     * 根据id查它的上级上级的上级
     *
     * @param id
     * @return
     */
    public List<GoodsType> selectParentById(String id) {
        if (id == null) {
            return null;
        }
        List<GoodsType> xlList = new ArrayList<>();
        GoodsType xielian = null;
        while ((xielian = selectParent(id)) != null) {
            xlList.add(xielian);
            id = xielian.getSuperior_id();
        }
        if (xlList.size() == 0) {
            return null;
        }
        return xlList;
    }

    public GoodsType selectParent(String id) {
        if (id == null) {
            return null;
        }
        return goodsTypeDAO.selectById(id);
    }
}
