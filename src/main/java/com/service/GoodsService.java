package com.service;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.*;
import com.pojo.*;
import com.util.StringUtil;

import jxl.format.UnderlineStyle;
import jxl.write.*;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @Author: ttw
 * @Description:商品管理的service层
 */
@Service
public class GoodsService extends ServiceImpl<GoodsDAO, Goods> {

    @Autowired
    private GoodsDAO goodsDAO;
    @Autowired
    private GoodsPictureDAO goodsPictureDAO;
    @Autowired
    private GoodsPictureEnDAO goodsPictureEnDAO;
    @Autowired
    private GoodsPictureTaiDAO goodsPictureTaiDAO;
    @Autowired
    private GoodsPictureKhDAO goodsPictureKhDAO;
    @Autowired
    private GoodsTypeDAO goodsTypeDAO;

    @Value("${upload.tmp.dir}")
    private String tmp;

    @Value("${upload.dir}")
    private String upload;

    /**
     * 添加商品
     *
     * @param goods 商品的对象
     * @return
     */
    public Goods addGoods(Goods goods) {
        String guige = goods.getGuige();
        if (StringUtil.isNotNull(guige)) {
            JSONObject jsonObject = JSONObject.parseObject(guige);
            JSONArray array = jsonObject.getJSONArray("values");
            if (array != null) {
                for (int i = 0; i < array.size(); i++) {
                    JSONObject value = array.getJSONObject(i);
                    String img = value.getString("img");
                    FileUtil.copy(tmp + img, upload + "images/goods/", true);
                    value.put("img", "images/goods/" + img);
                }
            }
            goods.setGuige(jsonObject.toJSONString());
        }
        String tu = goods.getGPicture();
        goods.setGPicture("images/goods/"+tu);
        goodsDAO.insert(goods);
        FileUtil.copy(tmp + tu,upload+"images/goods/",true);
        return goods;
    }

    /**
     * 新增商品图片
     *
     * @param goods  商品
     * @param image0 图片1
     * @param image1 图片2
     * @param image2 图片3
     * @param image3 图片4
     * @param image4 图片5
     * @param type 1:中文图片 2：英文图片 3：泰文图片
     */
    public void addGoodsImages(String id, String image0, String image1, String image2, String image3, String image4,int type) {
        List<String> list = Arrays.asList(image0, image1, image2, image3, image4);
        if (list == null || list.size() == 0) {
            return;
        }

        for (String img : list) {
            System.out.println(img);
            if (img == null) {
                continue;
            }

            if (type==1){
                GoodsPicture goodsimages = new GoodsPicture();
                goodsimages.setGPicture("images/goods/" + img);
                goodsimages.setGId(id);
                goodsimages.setCreatetime(new Date());
                goodsPictureDAO.insert(goodsimages);
            }else if (type==2){
                GoodsPictureEn goodsimages = new GoodsPictureEn();
                goodsimages.setGPicture("images/goods/" + img);
                goodsimages.setGId(id);
                goodsimages.setCreatetime(new Date());
                goodsPictureEnDAO.insert(goodsimages);
            }else if (type==3){
                GoodsPictureTai goodsimages = new GoodsPictureTai();
                goodsimages.setGPicture("images/goods/" + img);
                goodsimages.setGId(id);
                goodsimages.setCreatetime(new Date());
                goodsPictureTaiDAO.insert(goodsimages);
            }else if (type==4){
                GoodsPictureKh goodsPictureKh = new GoodsPictureKh();
                goodsPictureKh.setGPicture("images/goods/"+ img);
                goodsPictureKh.setGId(id);
                goodsPictureKh.setCreatetime(new Date());
                goodsPictureKhDAO.insert(goodsPictureKh);
            }

            new Thread() {
                @Override
                public void run() {
                    FileUtil.copy(tmp + img, upload + "images/goods/", true);
                }
            }.start();
        }

    }

    /**
     * APP查询商品
     *
     * @param page    页数
     * @param limit   每页条数
     * @param tuijian 推荐
     * @param sort    排序
     * @param gName   商品名字
     * @return
     */
    public IPage appSearchAllGoods(int page, int limit, int tuijian, int sort, String gName) {
        Page p = new Page(page, limit);
        QueryWrapper q = new QueryWrapper();
        q.eq("gStatus", 1);
        if (tuijian == 1) {
            q.eq("tuijian", 1);
        }
        if (StringUtil.isNotNull(gName)) {
            q.like("gName", gName);
        }
        if (sort == 0) {
            q.orderByDesc("weight");
        } else if (sort == 1) {
            q.orderByDesc("gSellnum");
        } else {
            q.orderByDesc("gTime");
        }
        IPage iPage = goodsDAO.selectPage(p, q);
        return iPage;
    }

    /**
     * 根据商品ID查询商品详情
     *
     * @param gid 商品id
     * @return
     */
    public Goods getGoodsById(String gid, String host, String yuyan) {
        Goods goods = goodsDAO.selectById(gid);
        if (goods != null) {
            String detail = "";
            GoodsType goodsType = goodsTypeDAO.selectById(goods.getGTypeId());
            if ("zh".equals(yuyan)){
               goodsType.setGname(goodsType.getGname());
            }else if ("en".equals(yuyan)){
               goodsType.setGname(goodsType.getGname_en());
            }else if ("th".equals(yuyan)){
               goodsType.setGname(goodsType.getGname_tai());
            }else if ("kh".equals(yuyan)){
                goodsType.setGname(goodsType.getGname_kh());
            }
            if (goodsType != null) {
                goods.setGoodsType(goodsType);
            }

            if ("en".equals(yuyan)) {
                if (StringUtil.isNotNull(goods.getGName_en())) {
                    List<GoodsPictureEn> list = goodsPictureEnDAO.selectList(new QueryWrapper<GoodsPictureEn>().eq("gid", gid));
                    goods.setGoodsPictureList(list);
                    detail=goods.getGDetail_en();

                    goods.setGName(goods.getGName_en());
                    goods.setGChandi(goods.getGChandi_en());
                    goods.setGPinpai(goods.getGPinpai_en());
                    goods.setGDanwei(goods.getGDanwei_en());
                    goods.setGJianjie(goods.getGJianjie_en());
                    goods.setGPrice(goods.getGPrice_en());
                    goods.setGrabPrice(goods.getGrabPrice_en());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_en());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_en());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_en());
                    goods.setOriginalPrice(goods.getOriginalPrice_en());
                } else {
                    List<GoodsPictureTai> list = goodsPictureTaiDAO.selectList(new QueryWrapper<GoodsPictureTai>().eq("gid", gid));
                    goods.setGoodsPictureList(list);
                    detail=goods.getGDetail_tai();

                    goods.setGName(goods.getGName_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            } else if ("th".equals(yuyan)) {
                List<GoodsPictureTai> list = goodsPictureTaiDAO.selectList(new QueryWrapper<GoodsPictureTai>().eq("gid", gid));
                goods.setGoodsPictureList(list);
                detail=goods.getGDetail_tai();

                goods.setGName(goods.getGName_tai());
                goods.setGChandi(goods.getGChandi_tai());
                goods.setGPinpai(goods.getGPinpai_tai());
                goods.setGDanwei(goods.getGDanwei_tai());
                goods.setGJianjie(goods.getGJianjie_tai());
                goods.setGPrice(goods.getGPrice_tai());
                goods.setGrabPrice(goods.getGrabPrice_tai());
                goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                goods.setOriginalPrice(goods.getOriginalPrice_tai());
            } else if ("zh".equals(yuyan)) {
                if (StringUtil.isNull(goods.getGName())) {
                    List<GoodsPictureTai> list = goodsPictureTaiDAO.selectList(new QueryWrapper<GoodsPictureTai>().eq("gid", gid));
                    goods.setGoodsPictureList(list);
                    detail=goods.getGDetail_tai();

                    goods.setGName(goods.getGName_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }else{
                    List<GoodsPicture> list = goodsPictureDAO.selectList(new QueryWrapper<GoodsPicture>().eq("gid", gid));
                    goods.setGoodsPictureList(list);
                    detail=goods.getGDetail();
                }
            }else if ("kh".equals(yuyan)) {
                if (StringUtil.isNotNull(goods.getGName_kh())) {
                    List<GoodsPictureKh> list = goodsPictureKhDAO.selectList(new QueryWrapper<GoodsPictureKh>().eq("gid", gid));
                    goods.setGoodsPictureList(list);
                    detail=goods.getGDetail_kh();

                    goods.setGName(goods.getGName_kh());
                    goods.setGChandi(goods.getGChandi_kh());
                    goods.setGPinpai(goods.getGPinpai_kh());
                    goods.setGDanwei(goods.getGDanwei_kh());
                    goods.setGJianjie(goods.getGJianjie_kh());
                    goods.setGPrice(goods.getGPrice_kh());
                    goods.setGrabPrice(goods.getGrabPrice_kh());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_kh());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_kh());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_kh());
                    goods.setOriginalPrice(goods.getOriginalPrice_kh());
                } else {
                    List<GoodsPictureTai> list = goodsPictureTaiDAO.selectList(new QueryWrapper<GoodsPictureTai>().eq("gid", gid));
                    goods.setGoodsPictureList(list);
                    detail=goods.getGDetail_tai();

                    goods.setGName(goods.getGName_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }

            if (detail!=null){
                if (detail.contains("/upload")){
                    goods.setGDetail(detail.replace("/upload", "http://" + host + "/upload"));
                }
            }
        }

        return goods;
    }

    /**
     * 后台分页查询所有商品
     *
     * @param name    商品名称
     * @param status  是否上架
     * @param tuijian 推荐
     * @param page    页数
     * @param size    每页大小
     * @return
     */
    public IPage searchGoods(String name, int status, int tuijian, int page, int size) {
        Page p = new Page(page, size);
        QueryWrapper<Goods> q = new QueryWrapper();
        if (StringUtil.isNotNull(name)) {
            q.like("gName", "%" + name + "%");
        }
        if (tuijian != -1) {
            q.eq("tuijian", tuijian);
        }
        if (status != -1) {
            if (status == 0) {
                q.eq("gStatus", status);
            } else {
                q.eq("gStatus", status);
            }
        }
        IPage result = goodsDAO.selectPage(p, q);
        List<Goods> list = result.getRecords();
        for (int i = 0; i < list.size(); i++) {
            GoodsType goodsType = goodsTypeDAO.selectById(list.get(i).getGTypeId());
            list.get(i).setGoodsType(goodsType);
        }
        result.setRecords(list);
        return result;
    }

    /**
     * 修改商品图片
     *
     * @param id 商品id
     * @param image0 图片1
     * @param image1 图片2
     * @param image2 图片3
     * @param image3 图片4
     * @param image4 图片5
     * @param type 1:中文图片，2：英文图片 3：泰语图片
     */
    public void updateGoodsImages(String id, String image0, String image1, String image2, String image3, String image4,int type) {
        List<String> list = Arrays.asList(image0, image1, image2, image3, image4);

        if (list == null || list.size() == 0) {
            return;
        }

        //删除原来的图片
        QueryWrapper q = new QueryWrapper();
        q.eq("gid", id);
        if (type==1){
            goodsPictureDAO.delete(q);
        }else if (type==2){
            goodsPictureEnDAO.delete(q);
        }else if (type==3){
            goodsPictureTaiDAO.delete(q);
        }else if (type==4){
            goodsPictureKhDAO.delete(q);
        }

        for (String img : list) {
            if (img == null) {
                continue;
            }

            if (type==1){
                //新增图片中文图片
                GoodsPicture goodsimages = new GoodsPicture();
                if (img.startsWith("images/")) {
                    goodsimages.setGPicture(img);
                } else {
                    goodsimages.setGPicture("images/goods/" + img);
                }
                goodsimages.setGId(id);
                goodsimages.setCreatetime(new Date());
                goodsPictureDAO.insert(goodsimages);
            }else if (type==2){
                //新增图片英文图片
                GoodsPictureEn goodsimages = new GoodsPictureEn();
                if (img.startsWith("images/")) {
                    goodsimages.setGPicture(img);
                } else {
                    goodsimages.setGPicture("images/goods/" + img);
                }
                goodsimages.setGId(id);
                goodsimages.setCreatetime(new Date());
                goodsPictureEnDAO.insert(goodsimages);
            }else if (type==3){
                //新增图片泰语图片
                GoodsPictureTai goodsimages = new GoodsPictureTai();
                if (img.startsWith("images/")) {
                    goodsimages.setGPicture(img);
                } else {
                    goodsimages.setGPicture("images/goods/" + img);
                }
                goodsimages.setGId(id);
                goodsimages.setCreatetime(new Date());
                goodsPictureTaiDAO.insert(goodsimages);
            }else if (type==4){
                GoodsPictureKh goodsPictureKh = new GoodsPictureKh();
                if (img.startsWith("images/")) {
                    goodsPictureKh.setGPicture(img);
                } else {
                    goodsPictureKh.setGPicture("images/goods/" + img);
                }
                goodsPictureKh.setGId(id);
                goodsPictureKh.setCreatetime(new Date());
                goodsPictureKhDAO.insert(goodsPictureKh);
            }


            File src = new File(tmp + img);
            if (src.exists()) {
                new Thread() {
                    @Override
                    public void run() {
                        FileUtil.copy(tmp + img, upload + "images/goods/", true);
                    }
                }.start();
            }
        }
    }

    /**
     * 获取推荐的商品
     *
     * @param type  推荐商品类型
     * @param yuyan 语言
     * @return
     */
    public List getAllTuijian(String yuyan, String type) {
        List<Goods> goodsList = goodsDAO.selectList(new QueryWrapper<Goods>().eq("tuijian", 1).eq("gTypeId", type).eq("gStatus",1).orderByDesc("weights"));
        if (goodsList.size() < 1)  // 如果此类别没有推荐商品则查询全部的
            goodsList = goodsDAO.selectList(new QueryWrapper<Goods>().eq("tuijian", 1).orderByDesc("weights"));
        if ("en".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNotNull(goods.getGName_en())) {
                    goods.setGName(goods.getGName_en());
                    goods.setGuige(goods.getGuige_en());
                    goods.setGChandi(goods.getGChandi_en());
                    goods.setGPinpai(goods.getGPinpai_en());
                    goods.setGDanwei(goods.getGDanwei_en());
                    goods.setGJianjie(goods.getGJianjie_en());
                    goods.setGPrice(goods.getGPrice_en());
                    goods.setGrabPrice(goods.getGrabPrice_en());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_en());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_en());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_en());
                    goods.setOriginalPrice(goods.getOriginalPrice_en());
                } else {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
        } else if ("th".equals(yuyan)) {
            for (Goods goods : goodsList) {
                goods.setGName(goods.getGName_tai());
                goods.setGuige(goods.getGuige_tai());
                goods.setGChandi(goods.getGChandi_tai());
                goods.setGPinpai(goods.getGPinpai_tai());
                goods.setGDanwei(goods.getGDanwei_tai());
                goods.setGJianjie(goods.getGJianjie_tai());
                goods.setGPrice(goods.getGPrice_tai());
                goods.setGrabPrice(goods.getGrabPrice_tai());
                goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                goods.setOriginalPrice(goods.getOriginalPrice_tai());
            }
        } else if ("zh".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNull(goods.getGName())) {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
        } else if ("kh".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNotNull(goods.getGName_kh())) {
                    goods.setGName(goods.getGName_kh());
                    goods.setGuige(goods.getGuige_kh());
                    goods.setGChandi(goods.getGChandi_kh());
                    goods.setGPinpai(goods.getGPinpai_kh());
                    goods.setGDanwei(goods.getGDanwei_kh());
                    goods.setGJianjie(goods.getGJianjie_kh());
                    goods.setGPrice(goods.getGPrice_kh());
                    goods.setGrabPrice(goods.getGrabPrice_kh());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_kh());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_kh());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_kh());
                    goods.setOriginalPrice(goods.getOriginalPrice_kh());
                } else {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
        }
        return goodsList;
    }

    /**
     * 分页查询，根据语言
     *
     * @param p     分页
     * @param qw    条件查询
     * @param yuyan 语言
     * @return
     */
    public IPage getPage(Page p, QueryWrapper qw, String yuyan) {
        IPage iPage = goodsDAO.selectPage(p, qw);
        List<Goods> goodsList = iPage.getRecords();
        if ("en".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNotNull(goods.getGName_en())) {
                    goods.setGName(goods.getGName_en());
                    goods.setGuige(goods.getGuige_en());
                    goods.setGChandi(goods.getGChandi_en());
                    goods.setGPinpai(goods.getGPinpai_en());
                    goods.setGDanwei(goods.getGDanwei_en());
                    goods.setGJianjie(goods.getGJianjie_en());
                    goods.setGPrice(goods.getGPrice_en());
                    goods.setGrabPrice(goods.getGrabPrice_en());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_en());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_en());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_en());
                    goods.setOriginalPrice(goods.getOriginalPrice_en());
                } else {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
            iPage.setRecords(goodsList);
        } else if ("th".equals(yuyan)) {
            for (Goods goods : goodsList) {
                goods.setGName(goods.getGName_tai());
                goods.setGuige(goods.getGuige_tai());
                goods.setGChandi(goods.getGChandi_tai());
                goods.setGPinpai(goods.getGPinpai_tai());
                goods.setGDanwei(goods.getGDanwei_tai());
                goods.setGJianjie(goods.getGJianjie_tai());
                goods.setGPrice(goods.getGPrice_tai());
                goods.setGrabPrice(goods.getGrabPrice_tai());
                goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                goods.setOriginalPrice(goods.getOriginalPrice_tai());
            }
            iPage.setRecords(goodsList);
        } else if ("zh".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNull(goods.getGName())) {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
            iPage.setRecords(goodsList);
        } else if ("kh".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNotNull(goods.getGName_kh())) {
                    goods.setGName(goods.getGName_kh());
                    goods.setGuige(goods.getGuige_kh());
                    goods.setGChandi(goods.getGChandi_kh());
                    goods.setGPinpai(goods.getGPinpai_kh());
                    goods.setGDanwei(goods.getGDanwei_kh());
                    goods.setGJianjie(goods.getGJianjie_kh());
                    goods.setGPrice(goods.getGPrice_kh());
                    goods.setGrabPrice(goods.getGrabPrice_kh());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_kh());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_kh());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_kh());
                    goods.setOriginalPrice(goods.getOriginalPrice_kh());
                } else {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
            iPage.setRecords(goodsList);
        }
        return iPage;
    }

    /**
     * 根据商品名称分页查询
     *
     * @param page      页数
     * @param limit     条数
     * @param goodsName 搜索内容
     * @param yuyan     语言
     * @return
     */
    public IPage getPageName(int page, int limit, String goodsName, String yuyan) {
        Page p = new Page(page, limit);
        QueryWrapper<Goods> qw = new QueryWrapper<>();
        qw.eq("gStatus", 1);
        if (StringUtil.isNotNull(goodsName)) {
            qw.and(wrapper ->wrapper.like("gName",goodsName).or().like("gName_en",goodsName).or().like("gName_tai",goodsName).or().like("gName_kh",goodsName));
        }
        qw.orderByDesc("weights");
        IPage iPage = goodsDAO.selectPage(p, qw);
        List<Goods> goodsList = iPage.getRecords();
        if ("en".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNotNull(goods.getGName_en())) {
                    goods.setGName(goods.getGName_en());
                    goods.setGuige(goods.getGuige_en());
                    goods.setGChandi(goods.getGChandi_en());
                    goods.setGPinpai(goods.getGPinpai_en());
                    goods.setGDanwei(goods.getGDanwei_en());
                    goods.setGJianjie(goods.getGJianjie_en());
                    goods.setGPrice(goods.getGPrice_en());
                    goods.setGrabPrice(goods.getGrabPrice_en());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_en());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_en());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_en());
                    goods.setOriginalPrice(goods.getOriginalPrice_en());
                } else {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
            iPage.setRecords(goodsList);
        } else if ("th".equals(yuyan)) {
            for (Goods goods : goodsList) {
                goods.setGName(goods.getGName_tai());
                goods.setGuige(goods.getGuige_tai());
                goods.setGChandi(goods.getGChandi_tai());
                goods.setGPinpai(goods.getGPinpai_tai());
                goods.setGDanwei(goods.getGDanwei_tai());
                goods.setGJianjie(goods.getGJianjie_tai());
                goods.setGPrice(goods.getGPrice_tai());
                goods.setGrabPrice(goods.getGrabPrice_tai());
                goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                goods.setOriginalPrice(goods.getOriginalPrice_tai());
            }
            iPage.setRecords(goodsList);
        } else if ("zh".equals(yuyan)) {
            for (Goods goods : goodsList) {
                if (StringUtil.isNull(goods.getGName())) {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
            iPage.setRecords(goodsList);
        }else if ("kh".equals(yuyan)){
            for (Goods goods : goodsList) {
                if (StringUtil.isNotNull(goods.getGName_kh())) {
                    goods.setGName(goods.getGName_kh());
                    goods.setGuige(goods.getGuige_kh());
                    goods.setGChandi(goods.getGChandi_kh());
                    goods.setGPinpai(goods.getGPinpai_kh());
                    goods.setGDanwei(goods.getGDanwei_kh());
                    goods.setGJianjie(goods.getGJianjie_kh());
                    goods.setGPrice(goods.getGPrice_kh());
                    goods.setGrabPrice(goods.getGrabPrice_kh());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_kh());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_kh());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_kh());
                    goods.setOriginalPrice(goods.getOriginalPrice_kh());
                } else {
                    goods.setGName(goods.getGName_tai());
                    goods.setGuige(goods.getGuige_tai());
                    goods.setGChandi(goods.getGChandi_tai());
                    goods.setGPinpai(goods.getGPinpai_tai());
                    goods.setGDanwei(goods.getGDanwei_tai());
                    goods.setGJianjie(goods.getGJianjie_tai());
                    goods.setGPrice(goods.getGPrice_tai());
                    goods.setGrabPrice(goods.getGrabPrice_tai());
                    goods.setGrabPriceDanwei(goods.getGrabPriceDanwei_tai());
                    goods.setGrabPriceSecond(goods.getGrabPriceSecond_tai());
                    goods.setGrabPriceSecondDanwei(goods.getGrabPriceSecondDanwei_tai());
                    goods.setOriginalPrice(goods.getOriginalPrice_tai());
                }
            }
            iPage.setRecords(goodsList);
        }
        return iPage;
    }

    /**
     * 导出excel
     * @param fileName
     * @param sheetName
     * @throws Exception
     */
    public void exportExcel(String fileName, String sheetName) throws Exception {
        WritableWorkbook workbook = jxl.Workbook.createWorkbook(new File(fileName));
        WritableSheet sheet = workbook.createSheet(sheetName, 0);
        WritableFont wf = new WritableFont(WritableFont.TIMES,18,WritableFont.BOLD,true);
        WritableCellFormat wcf = new WritableCellFormat(wf);
        wcf.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
        //设置title
        List<String> titles = new ArrayList();
        titles.add("序号");
        titles.add("商品名称");
        titles.add("商品类型名称");
        titles.add("商品价格(人民币)");
        titles.add("商品库存");
        titles.add("计量单位");
        titles.add("生产地");
        titles.add("品牌");
        titles.add("简介");
        titles.add("商品名称（泰语）");
        titles.add("商品价格(泰铢)");
        titles.add("计量单位（泰语）");
        titles.add("生产地（泰语）");
        titles.add("品牌（泰语）");
        titles.add("简介（泰语）");
        titles.add("商品名称（英语）");
        titles.add("商品价格(美元)");
        titles.add("计量单位（英语）");
        titles.add("生产地（英语）");
        titles.add("品牌（英语）");
        titles.add("简介（英语）");
        titles.add("商品名称（柬埔寨）");
        titles.add("商品价格(美元)");
        titles.add("计量单位（柬埔寨）");
        titles.add("生产地（柬埔寨）");
        titles.add("品牌（柬埔寨）");
        titles.add("简介（柬埔寨）");

        Label label=null;

        WritableCellFormat titleStyle = new WritableCellFormat(new WritableFont(WritableFont.createFont("微软雅黑"), 11, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.BLACK));
        titleStyle.setBackground(jxl.format.Colour.SEA_GREEN);
        titleStyle.setAlignment(jxl.format.Alignment.CENTRE);
        titleStyle.setVerticalAlignment(jxl.format.VerticalAlignment.getAlignment(1));
        titleStyle.setBorder(jxl.format.Border.NONE, jxl.format.BorderLineStyle.NONE, jxl.format.Colour.BLACK);

        WritableCellFormat colmStyle = new WritableCellFormat(new WritableFont(WritableFont.createFont("微软雅黑"), 10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE, jxl.format.Colour.BLACK));
//        title.setBackground(Colour.VERY_LIGHT_YELLOW);
        colmStyle.setAlignment(jxl.format.Alignment.CENTRE);
        colmStyle.setVerticalAlignment(jxl.format.VerticalAlignment.getAlignment(1));
        colmStyle.setBorder(jxl.format.Border.NONE, jxl.format.BorderLineStyle.NONE, Colour.BLACK);

        for(int i = 0;i < titles.size();i++){
            //x,y,第一行的列名
            label=new Label(i,0,titles.get(i),titleStyle);
            //7：添加单元格
            sheet.addCell((WritableCell) label);
        }

        workbook.write();
        workbook.close();
    }

    /**
     * 导入Excel
     *
     * @param file
     * @return
     */
    public Map<String, String> importExcelByGoods(MultipartFile file, List<Goods> goodsList) {
        Map<String, String> resultMap = new HashMap<>();
        List<String> bhList = new ArrayList<>();
        try {
            //格式化日期
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
            InputStream is = file.getInputStream();
            Workbook rwb = WorkbookFactory.create(is);
            Sheet sheet = rwb.getSheetAt(0);
            int j = 0;
            for (int r = 1; r <= sheet.getLastRowNum(); r++) {//r = 1表示从第2行开始循环 如果你的第2行开始是数据
                Row rs = sheet.getRow(r);//通过sheet表单对象得到 行对象
                if (rs == null) {
                    continue;
                }
                Goods goods = new Goods();
                int i = 1;
                //第一个是列数，第二个是行数
                String num = "";
                if (rs.getCell(0).getCellTypeEnum() == CellType.NUMERIC) {
                    Double num1 = rs.getCell(0).getNumericCellValue(); //默认最左边编号也算一列 所以这里得j++
                    Integer num2 = num1.intValue();
                    num = String.valueOf(num2);
                } else if (rs.getCell(0).getCellTypeEnum() == CellType.FORMULA) {
                    FormulaEvaluator hssfCell = rwb.getCreationHelper().createFormulaEvaluator();
                    CellValue tempCellValue = hssfCell.evaluate(rs.getCell(0));
                    Double num2 = tempCellValue.getNumberValue();
                    num = String.valueOf(num2);
                } else {
                    num = rs.getCell(0).getStringCellValue();
                }

                String name = null;
                if (rs.getCell(1) != null && rs.getCell(1).getCellTypeEnum() != null) {
                    if (rs.getCell(1).getCellTypeEnum() == CellType.NUMERIC) {
                        Double name1 = rs.getCell(1).getNumericCellValue();
                        Integer name2 = name1.intValue();
                        name = String.valueOf(name2);
                    } else {
                        name = rs.getCell(1).getStringCellValue();
                    }
                } else {
                    resultMap.put("error", "序号：" + num + "<br>错误信息：<span style='color:red;'>商品名称不能为空</span>");
                    return resultMap;
                }

                if (isEmptyString(name)) {
                    resultMap.put("error", "序号：" + num + "<br>错误信息：<span style='color:red;'>商品名称不能为空</span>");
                    return resultMap;
                }

                int nameCount = goodsDAO.selectList(new QueryWrapper<Goods>().eq("gName", name)).size();
                if (nameCount > 0) {
                    resultMap.put("error", "序号：" + num + "<br>商品名称：" + "<span style='color:red;'>" + name + "</span>" + "<br>错误信息：<span style='color:red;'>商品名称已存在</span>");
                    return resultMap;
                }

                String gName = null;//商品名称
                if (rs.getCell(1) != null && rs.getCell(1).getCellTypeEnum() != null) {
                    if (rs.getCell(1).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gName1 = rs.getCell(1).getNumericCellValue();
                        Integer gName2 = gName1.intValue();
                        gName = String.valueOf(gName2);
                    } else {
                        gName = rs.getCell(1).getStringCellValue();
                    }
                }

                String gTypeId = null;//商品类型 id
                if (rs.getCell(2) != null && rs.getCell(2).getCellTypeEnum() != null) {
                    if (rs.getCell(2).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gTypeId1 = rs.getCell(2).getNumericCellValue();
                        Integer gTypeId2 = gTypeId1.intValue();
                        String typename = String.valueOf(gTypeId2);
                        GoodsType goodsType = goodsTypeDAO.selectOne(new QueryWrapper<GoodsType>().eq("gname", typename));
                        if (goodsType != null) {
                            if (goodsType.getGrade() != 3) {
                                gTypeId = goodsType.getId();
                            } else {
                                resultMap.put("error", "序号：" + num + "<br>商品类型名称：" + "<span style='color:red;'>" + typename + "</span>" + "<br>错误信息：<span style='color:red;'>该商品类型不是第三级类型</span>");
                                return resultMap;
                            }
                        } else {
                            resultMap.put("error", "序号：" + num + "<br>商品类型名称：" + "<span style='color:red;'>" + typename + "</span>" + "<br>错误信息：<span style='color:red;'>该商品类型名称不存在</span>");
                            return resultMap;
                        }
                    } else {
                        String typename2 = rs.getCell(2).getStringCellValue();
                        GoodsType goodsType = goodsTypeDAO.selectOne(new QueryWrapper<GoodsType>().eq("gname", typename2));
                        if (goodsType != null) {
                            if (goodsType.getGrade() == 3) {
                                gTypeId = goodsType.getId();
                            } else {
                                resultMap.put("error", "序号：" + num + "<br>商品类型名称：" + "<span style='color:red;'>" + typename2 + "</span>" + "<br>错误信息：<span style='color:red;'>该商品类型不是第三级类型</span>");
                                return resultMap;
                            }
                        } else {
                            resultMap.put("error", "序号：" + num + "<br>商品类型名称：" + "<span style='color:red;'>" + typename2 + "</span>" + "<br>错误信息：<span style='color:red;'>该商品类型名称不存在</span>");
                            return resultMap;
                        }
                    }
                }

                String gPrice = null;//商品价格（人民币）
                if (rs.getCell(3) != null && rs.getCell(3).getCellTypeEnum() != null) {
                    if (rs.getCell(3).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice1 = rs.getCell(3).getNumericCellValue();
                        Integer gPrice2 = gPrice1.intValue();
                        gPrice = String.valueOf(gPrice2);
                    } else {
                        gPrice = rs.getCell(3).getStringCellValue();
                    }
                }

                String originalPrice = null;//原价
                if (rs.getCell(4) != null && rs.getCell(4).getCellTypeEnum() != null) {
                    if (rs.getCell(4).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gKucun1 = rs.getCell(4).getNumericCellValue();
                        Integer gKucun2 = gKucun1.intValue();
                        originalPrice = String.valueOf(gKucun2);
                    } else {
                        originalPrice = rs.getCell(4).getStringCellValue();
                    }
                }

                String gKucun = null;//库存
                if (rs.getCell(5) != null && rs.getCell(5).getCellTypeEnum() != null) {
                    if (rs.getCell(5).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gKucun1 = rs.getCell(5).getNumericCellValue();
                        Integer gKucun2 = gKucun1.intValue();
                        gKucun = String.valueOf(gKucun2);
                    } else {
                        gKucun = rs.getCell(5).getStringCellValue();
                    }
                }

                String gDanwei = null;//计量单位
                if (rs.getCell(6) != null && rs.getCell(6).getCellTypeEnum() != null) {
                    if (rs.getCell(6).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gDanwei1 = rs.getCell(6).getNumericCellValue();
                        Integer gDanwei2 = gDanwei1.intValue();
                        gDanwei = String.valueOf(gDanwei2);
                    } else {
                        gDanwei = rs.getCell(6).getStringCellValue();
                    }
                }


                String gChandi = null;//产地
                if (rs.getCell(7) != null && rs.getCell(7).getCellTypeEnum() != null) {
                    if (rs.getCell(7).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gChandi1 = rs.getCell(7).getNumericCellValue();
                        Integer gChandi2 = gChandi1.intValue();
                        gChandi = String.valueOf(gChandi2);
                    } else {
                        gChandi = rs.getCell(7).getStringCellValue();
                    }
                }

                String gPinpai = null;//品牌
                if (rs.getCell(8) != null && rs.getCell(8).getCellTypeEnum() != null) {
                    if (rs.getCell(8).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPinpai1 = rs.getCell(8).getNumericCellValue();
                        Integer gPinpai2 = gPinpai1.intValue();
                        gPinpai = String.valueOf(gPinpai2);
                    } else {
                        gPinpai = rs.getCell(8).getStringCellValue();
                    }
                }

                String gJianjie = null; //商品简介
                if (rs.getCell(9) != null && rs.getCell(9).getCellTypeEnum() != null) {
                    if (rs.getCell(9).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gJianjie1 = rs.getCell(9).getNumericCellValue();
                        Integer gJianjie2 = gJianjie1.intValue();
                        gJianjie = String.valueOf(gJianjie2);
                    } else {
                        gJianjie = rs.getCell(9).getStringCellValue();
                    }
                }

                String gName_tai = null;//商品名称（泰语）
                if (rs.getCell(10) != null && rs.getCell(10).getCellTypeEnum() != null) {
                    if (rs.getCell(10).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gName_tai1 = rs.getCell(10).getNumericCellValue();
                        Integer gName_tai2 = gName_tai1.intValue();
                        gName_tai = String.valueOf(gName_tai2);
                    } else {
                        gName_tai = rs.getCell(10).getStringCellValue();
                    }
                }

                String gPrice_tai = null;//商品价格（泰铢）
                if (rs.getCell(11) != null && rs.getCell(11).getCellTypeEnum() != null) {
                    if (rs.getCell(11).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice_tai1 = rs.getCell(11).getNumericCellValue();
                        Integer gPrice_tai2 = gPrice_tai1.intValue();
                        gPrice_tai = String.valueOf(gPrice_tai2);
                    } else {
                        gPrice_tai = rs.getCell(11).getStringCellValue();
                    }
                }

                String originalPrice_tai = null;//商品价格（泰铢）
                if (rs.getCell(12) != null && rs.getCell(12).getCellTypeEnum() != null) {
                    if (rs.getCell(12).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice_tai1 = rs.getCell(12).getNumericCellValue();
                        Integer gPrice_tai2 = gPrice_tai1.intValue();
                        originalPrice_tai = String.valueOf(gPrice_tai2);
                    } else {
                        originalPrice_tai = rs.getCell(12).getStringCellValue();
                    }
                }

                String gDanwei_tai = null;//商品计量单位（泰语）
                if (rs.getCell(13) != null && rs.getCell(13).getCellTypeEnum() != null) {
                    if (rs.getCell(13).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gDanwei_tai1 = rs.getCell(13).getNumericCellValue();
                        Integer gDanwei_tai2 = gDanwei_tai1.intValue();
                        gDanwei_tai = String.valueOf(gDanwei_tai2);
                    } else {
                        gDanwei_tai = rs.getCell(13).getStringCellValue();
                    }
                }

                String gChandi_tai = null;//商品计量单位（泰语）
                if (rs.getCell(14) != null && rs.getCell(14).getCellTypeEnum() != null) {
                    if (rs.getCell(14).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gChandi_tai1 = rs.getCell(14).getNumericCellValue();
                        Integer gChandi_tai2 = gChandi_tai1.intValue();
                        gChandi_tai = String.valueOf(gChandi_tai2);
                    } else {
                        gChandi_tai = rs.getCell(14).getStringCellValue();
                    }
                }

                String gPinpai_tai = null;//商品品牌（泰语）
                if (rs.getCell(15) != null && rs.getCell(15).getCellTypeEnum() != null) {
                    if (rs.getCell(15).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPinpai_tai1 = rs.getCell(15).getNumericCellValue();
                        Integer gPinpai_tai2 = gPinpai_tai1.intValue();
                        gPinpai_tai = String.valueOf(gPinpai_tai2);
                    } else {
                        gPinpai_tai = rs.getCell(15).getStringCellValue();
                    }
                }

                String gJianjie_tai = null;//商品简介（泰语）
                if (rs.getCell(16) != null && rs.getCell(16).getCellTypeEnum() != null) {
                    if (rs.getCell(16).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gJianjie_tai1 = rs.getCell(16).getNumericCellValue();
                        Integer gJianjie_tai2 = gJianjie_tai1.intValue();
                        gJianjie_tai = String.valueOf(gJianjie_tai2);
                    } else {
                        gJianjie_tai = rs.getCell(16).getStringCellValue();
                    }
                }

                String gName_en = null;//商品名称（英语）
                if (rs.getCell(17) != null && rs.getCell(17).getCellTypeEnum() != null) {
                    if (rs.getCell(17).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gName_en1 = rs.getCell(17).getNumericCellValue();
                        Integer gName_en2 = gName_en1.intValue();
                        gName_en = String.valueOf(gName_en2);
                    } else {
                        gName_en = rs.getCell(17).getStringCellValue();
                    }
                }

                String gPrice_en = null;//商品价格（美元）
                if (rs.getCell(18) != null && rs.getCell(18).getCellTypeEnum() != null) {
                    if (rs.getCell(18).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice_en1 = rs.getCell(18).getNumericCellValue();
                        Integer gPrice_en2 = gPrice_en1.intValue();
                        gPrice_en = String.valueOf(gPrice_en2);
                    } else {
                        gPrice_en = rs.getCell(18).getStringCellValue();
                    }
                }

                String originalPrice_en = null;//商品价格（美元）
                if (rs.getCell(19) != null && rs.getCell(19).getCellTypeEnum() != null) {
                    if (rs.getCell(19).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice_en1 = rs.getCell(19).getNumericCellValue();
                        Integer gPrice_en2 = gPrice_en1.intValue();
                        originalPrice_en = String.valueOf(gPrice_en2);
                    } else {
                        originalPrice_en = rs.getCell(19).getStringCellValue();
                    }
                }

                String gDanwei_en = null;//商品计量单位（英语）
                if (rs.getCell(20) != null && rs.getCell(20).getCellTypeEnum() != null) {
                    if (rs.getCell(20).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gDanwei_en1 = rs.getCell(20).getNumericCellValue();
                        Integer gDanwei_en2 = gDanwei_en1.intValue();
                        gDanwei_en = String.valueOf(gDanwei_en2);
                    } else {
                        gDanwei_en = rs.getCell(20).getStringCellValue();
                    }
                }

                String gChandi_en = null;//商品计量单位（英语）
                if (rs.getCell(21) != null && rs.getCell(21).getCellTypeEnum() != null) {
                    if (rs.getCell(21).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gChandi_en1 = rs.getCell(21).getNumericCellValue();
                        Integer gChandi_en2 = gChandi_en1.intValue();
                        gChandi_en = String.valueOf(gChandi_en2);
                    } else {
                        gChandi_en = rs.getCell(21).getStringCellValue();
                    }
                }

                String gPinpai_en = null;//商品品牌（英语）
                if (rs.getCell(22) != null && rs.getCell(22).getCellTypeEnum() != null) {
                    if (rs.getCell(22).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPinpai_en1 = rs.getCell(22).getNumericCellValue();
                        Integer gPinpai_en2 = gPinpai_en1.intValue();
                        gPinpai_en = String.valueOf(gPinpai_en2);
                    } else {
                        gPinpai_en = rs.getCell(22).getStringCellValue();
                    }
                }

                String gJianjie_en = null;//商品简介（英语）
                if (rs.getCell(23) != null && rs.getCell(23).getCellTypeEnum() != null) {
                    if (rs.getCell(23).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gJianjie_en1 = rs.getCell(23).getNumericCellValue();
                        Integer gJianjie_en2 = gJianjie_en1.intValue();
                        gJianjie_en = String.valueOf(gJianjie_en2);
                    } else {
                        gJianjie_en = rs.getCell(23).getStringCellValue();
                    }
                }

                String gName_kh = null;//商品名称（柬埔寨）
                if (rs.getCell(24) != null && rs.getCell(24).getCellTypeEnum() != null) {
                    if (rs.getCell(24).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gName_en1 = rs.getCell(24).getNumericCellValue();
                        Integer gName_en2 = gName_en1.intValue();
                        gName_kh = String.valueOf(gName_en2);
                    } else {
                        gName_kh = rs.getCell(24).getStringCellValue();
                    }
                }

                String gPrice_kh = null;//商品价格（美元）
                if (rs.getCell(25) != null && rs.getCell(25).getCellTypeEnum() != null) {
                    if (rs.getCell(25).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice_en1 = rs.getCell(25).getNumericCellValue();
                        Integer gPrice_en2 = gPrice_en1.intValue();
                        gPrice_kh = String.valueOf(gPrice_en2);
                    } else {
                        gPrice_kh = rs.getCell(25).getStringCellValue();
                    }
                }

                String originalPrice_kh = null;//商品价格（美元）
                if (rs.getCell(26) != null && rs.getCell(26).getCellTypeEnum() != null) {
                    if (rs.getCell(26).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPrice_en1 = rs.getCell(26).getNumericCellValue();
                        Integer gPrice_en2 = gPrice_en1.intValue();
                        originalPrice_kh = String.valueOf(gPrice_en2);
                    } else {
                        originalPrice_kh = rs.getCell(26).getStringCellValue();
                    }
                }

                String gDanwei_kh = null;//商品计量单位（柬埔寨）
                if (rs.getCell(27) != null && rs.getCell(27).getCellTypeEnum() != null) {
                    if (rs.getCell(27).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gDanwei_en1 = rs.getCell(27).getNumericCellValue();
                        Integer gDanwei_en2 = gDanwei_en1.intValue();
                        gDanwei_kh = String.valueOf(gDanwei_en2);
                    } else {
                        gDanwei_kh = rs.getCell(27).getStringCellValue();
                    }
                }

                String gChandi_kh = null;//商品计量单位（英语）
                if (rs.getCell(28) != null && rs.getCell(28).getCellTypeEnum() != null) {
                    if (rs.getCell(28).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gChandi_en1 = rs.getCell(28).getNumericCellValue();
                        Integer gChandi_en2 = gChandi_en1.intValue();
                        gChandi_kh = String.valueOf(gChandi_en2);
                    } else {
                        gChandi_kh = rs.getCell(28).getStringCellValue();
                    }
                }

                String gPinpai_kh = null;//商品品牌（英语）
                if (rs.getCell(29) != null && rs.getCell(29).getCellTypeEnum() != null) {
                    if (rs.getCell(29).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gPinpai_en1 = rs.getCell(29).getNumericCellValue();
                        Integer gPinpai_en2 = gPinpai_en1.intValue();
                        gPinpai_kh = String.valueOf(gPinpai_en2);
                    } else {
                        gPinpai_kh = rs.getCell(29).getStringCellValue();
                    }
                }

                String gJianjie_kh = null;//商品简介（英语）
                if (rs.getCell(30) != null && rs.getCell(30).getCellTypeEnum() != null) {
                    if (rs.getCell(30).getCellTypeEnum() == CellType.NUMERIC) {
                        Double gJianjie_en1 = rs.getCell(30).getNumericCellValue();
                        Integer gJianjie_en2 = gJianjie_en1.intValue();
                        gJianjie_kh = String.valueOf(gJianjie_en2);
                    } else {
                        gJianjie_kh = rs.getCell(30).getStringCellValue();
                    }
                }

                goods.setGName(gName);
                goods.setGTypeId(gTypeId);
                goods.setGPrice(new BigDecimal(gPrice));
                goods.setOriginalPrice(new BigDecimal(originalPrice));
                goods.setGDanwei(gDanwei);
                goods.setGChandi(gChandi);
                goods.setGPinpai(gPinpai);
                goods.setGJianjie(gJianjie);
                goods.setWeights(1);
                goods.setGStatus(0);
                goods.setGName_tai(gName_tai);
                goods.setGDanwei_tai(gDanwei_tai);
                goods.setGChandi_tai(gChandi_tai);
                goods.setGPinpai_tai(gPinpai_tai);
                goods.setGJianjie_tai(gJianjie_tai);
                goods.setGName_en(gName_en);
                goods.setGDanwei_en(gDanwei_en);
                goods.setGJianjie_en(gJianjie_en);
                goods.setGPinpai_en(gPinpai_en);
                goods.setGChandi_en(gChandi_en);
                goods.setGName_kh(gName_kh);
                goods.setGDanwei_kh(gDanwei_kh);
                goods.setGJianjie_kh(gJianjie_kh);
                goods.setGPinpai_kh(gPinpai_kh);
                goods.setGChandi_kh(gChandi_kh);
                goods.setCreatetime(new Date());
                goods.setGPrice_en(new BigDecimal(gPrice_en));
                goods.setOriginalPrice_en(new BigDecimal(originalPrice_en));
                goods.setGPrice_tai(new BigDecimal(gPrice_tai));
                goods.setOriginalPrice_tai(new BigDecimal(originalPrice_tai));
                goods.setGPrice_kh(new BigDecimal(gPrice_kh));
                goods.setOriginalPrice_kh(new BigDecimal(originalPrice_kh));
                goods.setGKucun(Integer.parseInt(gKucun));
                goodsList.add(goods);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        resultMap.put("sucess", "sucess");
        return resultMap;
    }

    /**
     * 判断字符串是否为空
     *
     * @param string
     * @return boolean
     */
    public static boolean isEmptyString(String string) {
        if (string == null || string.length() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
