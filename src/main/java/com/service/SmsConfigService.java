package com.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.SmsConfigDAO;
import com.pojo.SmsConfig;
import org.springframework.stereotype.Service;

/**
 * @Author:Liao
 * @Discription:用户
 */
@Service
public class SmsConfigService extends ServiceImpl<SmsConfigDAO, SmsConfig> {

}
