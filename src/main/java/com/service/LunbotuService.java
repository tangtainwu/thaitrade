package com.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.LunbotuDAO;
import com.pojo.Lunbotu;
import org.springframework.stereotype.Service;

/**
 * @Author: ttw
 * @Description: 轮播图的service层
 */
@Service
public class LunbotuService extends ServiceImpl<LunbotuDAO, Lunbotu> {
}
