package com.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.GoodsPictureDAO;
import com.dao.GoodsPictureEnDAO;
import com.dao.GoodsPictureKhDAO;
import com.dao.GoodsPictureTaiDAO;
import com.pojo.GoodsPicture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: T-bao
 * @Date: 2021-8-6 10:59
 * @Description:
 */
@Service
public class GoodsImagesService extends ServiceImpl<GoodsPictureDAO, GoodsPicture> {

    @Autowired
    private GoodsPictureDAO goodsImagesDAO;
    @Autowired
    private GoodsPictureEnDAO goodsPictureEnDAO;
    @Autowired
    private GoodsPictureTaiDAO goodsPictureTaiDAO;
    @Autowired
    private GoodsPictureKhDAO goodsPictureKhDAO;

    /**
     * 根据商品id得到图片
     *
     * @param goodsID 商品id
     * @return
     */
    public List getGoodsImages(String goodsID) {
        QueryWrapper q = new QueryWrapper();
        q.eq("gId", goodsID);
        List list = goodsImagesDAO.selectList(q);
        return list;
    }

    /**
     * 根据商品id得到英文图片
     *
     * @param goodsID 商品id
     * @return
     */
    public List getGoodsImagesEn(String goodsID) {
        QueryWrapper q = new QueryWrapper();
        q.eq("gId", goodsID);
        List list = goodsPictureEnDAO.selectList(q);
        return list;
    }

    /**
     * 根据商品id得到泰语图片
     *
     * @param goodsID 商品id
     * @return
     */
    public List getGoodsImagesTai(String goodsID) {
        QueryWrapper q = new QueryWrapper();
        q.eq("gId", goodsID);
        List list = goodsPictureTaiDAO.selectList(q);
        return list;
    }

    /**
     * 根据商品id得到柬埔寨图片
     *
     * @param goodsID
     * @return
     */
    public List getGoodsImagesKh(String goodsID){
        QueryWrapper q = new QueryWrapper();
        q.eq("gId",goodsID);
        List list = goodsPictureKhDAO.selectList(q);
        return list;
    }
}
