package com.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.AdvertiseDAO;
import com.pojo.Advertise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: ttw
 * @Date: 2022-02-21
 * @Description:
 */
@Service
public class AdvertiseService extends ServiceImpl<AdvertiseDAO, Advertise> {

    @Autowired
    private AdvertiseDAO advertiseDAO;

    /**
     * 分页查询广告
     * @param page 页数
     * @param limit 条数
     * @param status 状态
     * @return
     */
    public IPage<Advertise> getPage(int page ,int limit,int status){
        Page p = new Page(page,limit);
        QueryWrapper<Advertise> qw = new QueryWrapper<Advertise>();
        if (status!=-1){
            qw.eq("status",status);
        }
//        qw.orderByAsc("createTime");
        return advertiseDAO.selectPage(p,qw);
    }

    /**
     * 获取状态为1的个数
     * @return
     */
    public int getStatusNumbers(){
        int number = advertiseDAO.selectCount(new QueryWrapper<Advertise>().eq("status",1));
        return number;
    }

    /**
     * 将状态修改为0
     * @return
     */
    public int updateStatus(){
        Advertise advertise = new Advertise();
        advertise.setStatus(0);
        UpdateWrapper uw = new UpdateWrapper();
        return advertiseDAO.update(advertise,uw);
    }

    /**
     * 获取广告
     * @param yuyan
     * @return
     */
    public Advertise getAdvertise(String yuyan){
        QueryWrapper<Advertise> qw = new QueryWrapper<Advertise>();
        qw.eq("status",1);
        qw.last("limit 1");
        Advertise advertise = advertiseDAO.selectOne(qw);
        if (advertise!=null){
            if ("en".equals(yuyan)){
                advertise.setPicture(advertise.getPicture_en());
            }else if ("th".equals(yuyan)){
                advertise.setPicture(advertise.getPicture_tai());
            }else if ("kh".equals(yuyan)){
                advertise.setPicture(advertise.getPicture_kh());
            }
        }
        return advertise;
    }
}
