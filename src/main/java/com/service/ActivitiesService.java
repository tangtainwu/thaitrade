package com.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.ActivitiesDAO;
import com.pojo.Activities;
import com.pojo.GoodsPicture;
import com.pojo.GoodsPictureEn;
import com.pojo.GoodsPictureTai;
import com.sun.javafx.webkit.UtilitiesImpl;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: ttw
 * @Date: 2021-10-7 10:31
 * @Description:
 */
@Service
public class ActivitiesService extends ServiceImpl<ActivitiesDAO, Activities> {
    @Autowired
    private ActivitiesDAO activitiesDAO;

    /**
     * 新增活动
     * @param title 标题
     * @param detail 详情
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    public String addActivities(String title,String detail,String startTime,String endTime,Integer status,String title_en,String title_tai,String title_kh,int xianshi,String host,String detail_en,String detail_tai,String detail_kh) throws ParseException {
        List<Activities> activitiesList=activitiesDAO.selectList(new QueryWrapper<Activities>().eq("title", title));
        if (activitiesList.size()>0){
            return "标题重复";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date1=simpleDateFormat.parse(startTime);
        Date date2=simpleDateFormat.parse(endTime);
        if (date1.getTime() > date2.getTime()){
            return "开始时间不能大于结束时间";
        }
        Activities activities=new Activities();
        activities.setTitle(title);
        activities.setTitle_en(title_en);
        activities.setTitle_tai(title_tai);
        activities.setDetail(detail);
        activities.setDetail_en(detail_en);
        activities.setDetail_tai(detail_tai);
        activities.setCreateTime(new Date());
        activities.setStartTime(startTime);
        activities.setEndTime(endTime);
        activities.setStatus(status);
        activities.setXianshi(xianshi);
        activities.setTitle_kh(title_kh);
        activities.setDetail_kh(detail_kh);
        if(activitiesDAO.insert(activities)>0){
            activities.setUrl("http://"+host+"/home/activity.html?id="+activities.getId());
            if (activitiesDAO.updateById(activities)>0){
                return null;
            }else{
              return "网址设置失败";
            }

        }else{
            return "新增失败";
        }
    }

    /**
     * 分页查询
     * @param page 页数
     * @param limit 条数
     * @param title 标题
     * @param status 状态
     * @return
     */
    public IPage getpage(int page,int limit,String title,int status){
        Page p = new Page(page,limit);
        IPage iPage = activitiesDAO.getpage(p,title,status);
        List list=iPage.getRecords();
        return iPage;
    }

    /**
     * 根据id查询
     * @param id 活动id
     * @param yuyan 语言
     * @return
     */
    public Activities getActivitiesById(String id,String yuyan){
        Activities activities=activitiesDAO.getByid(id);
        if ("en".equals(yuyan)){
            if (StringUtil.isNull(activities.getTitle_en())){
                activities.setTitle(activities.getTitle_tai());
                activities.setDetail_en(activities.getDetail_tai());
            }
            activities.setTitle(activities.getTitle_en());
            activities.setDetail(activities.getDetail_en());
        }else if ("th".equals(yuyan)){
            if (StringUtil.isNull(activities.getTitle_tai())){
                activities.setTitle(activities.getTitle());
                activities.setDetail_en(activities.getDetail());
            }
            activities.setTitle(activities.getTitle_tai());
            activities.setDetail(activities.getDetail_tai());
        }else if ("kh".equals(yuyan)){
            if (StringUtil.isNull(activities.getTitle_kh())){
                activities.setTitle(activities.getTitle());
                activities.setDetail_en(activities.getDetail());
            }
            activities.setTitle(activities.getTitle_kh());
            activities.setDetail(activities.getDetail_kh());
        }else{
            if (StringUtil.isNull(activities.getTitle())){
                activities.setTitle(activities.getTitle_tai());
                activities.setDetail(activities.getDetail_tai());
            }
            activities.setTitle(activities.getTitle());
            activities.setDetail(activities.getDetail());
        }
        return activities;
    }

    /**
     * 修改活动的信息
     * @param id 活动id
     * @param title 标题
     * @param detail 详情
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param status 状态
     * @return
     */
    public String updateActivities(String id,String title,String detail,String startTime,String endTime,int status,String title_en,String title_tai,String title_kh,int xianshi,String detail_en,String detail_tai,String detail_kh) throws ParseException {
        Activities activities=activitiesDAO.getByid(id);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date1=simpleDateFormat.parse(startTime);
        Date date2=simpleDateFormat.parse(endTime);
        if (date1.getTime() > date2.getTime()){
            return "开始时间不能大于结束时间";
        }
        activities.setTitle(title);
        activities.setTitle_tai(title_tai);
        activities.setTitle_en(title_en);
        activities.setStatus(status);
        activities.setDetail(detail);
        activities.setDetail_en(detail_en);
        activities.setDetail_tai(detail_tai);
        activities.setStartTime(startTime);
        activities.setEndTime(endTime);
        activities.setXianshi(xianshi);
        activities.setDetail_kh(detail_kh);
        activities.setTitle_kh(title_kh);
        int n= activitiesDAO.updateById(activities);
        if (n>0){
            return null;
        }else{
            return "修改失败";
        }
    }

    /**
     * 获取该月的活动
     * @param month 月份
     * @param yuyan 语言
     * @return
     */
    public List<Activities> getActivitiesByTime(int month,String yuyan,int year) throws ParseException {
        List<Activities> activitiesList=activitiesDAO.selectList(new QueryWrapper<Activities>().eq("status",1).eq("xianshi",1));
        List<Activities> list=new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (Activities activities:activitiesList) {
            Date date1=simpleDateFormat.parse(activities.getStartTime());
            Date date2=simpleDateFormat.parse(activities.getEndTime());
            int year1 = Integer.parseInt(activities.getStartTime().substring(0,4));
            System.out.println(year1);
            int year2 = Integer.parseInt(activities.getEndTime().substring(0,4));
            if (year>=year1 && year<=year2){
                if (date1.getMonth()<=month && date2.getMonth()>=month){
                    list.add(activities);
                }
            }
        }
        if ("zh".equals(yuyan)){
            for (Activities activities :list) {
                if (StringUtil.isNull(activities.getTitle())){
                    activities.setTitle(activities.getTitle_tai());
                }
            }
        }else if ("en".equals(yuyan)){
            for (Activities activities:list) {
                if (StringUtil.isNull(activities.getTitle_en())){
                    activities.setTitle(activities.getTitle_tai());
                }else{
                    activities.setTitle(activities.getTitle_en());
                }
            }
        }else if("th".equals(yuyan)){
            for (Activities activities :list) {
                activities.setTitle(activities.getTitle_tai());
            }
        }else if ("kh".equals(yuyan)){
            for (Activities activities :list) {
                if (StringUtil.isNull(activities.getTitle_kh())){
                    activities.setTitle(activities.getTitle_tai());
                }else{
                    activities.setTitle(activities.getTitle_kh());
                }
            }
        }
        return list;
    }
}
