package com.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.UsersDAO;
import com.pojo.Users;
import com.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author:Liao
 * @Discription:用户
 */
@Service
public class UsersService extends ServiceImpl<UsersDAO, Users> {

    @Autowired
    private UsersDAO usersDAO;


    /**
     * 分页查询用户
     * @param page 页数
     * @param limit 条数
     * @param uname 用户名字
     * @return
     */
    public IPage selectPage(int page,int limit,String uname,int type){
        Page p = new Page(page,limit);
        QueryWrapper<Users> qw = new QueryWrapper<Users>();
        if (StringUtil.isNotNull(uname)){
            qw.like("uname",uname);
        }
        if (type!=-1){
            qw.eq("type",type);
        }
        qw.orderByAsc("createTime");
        IPage iPage = usersDAO.selectPage(p,qw);
        return iPage;
    }

}
