package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ttw
 * @Date: 2021-10-7 10:30
 * @Description:
 */
@Data
@TableName(value = "activities")
public class Activities implements Serializable{
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String title;           //标题
    private String detail;          //活动详情
    @JsonFormat(pattern = "yyyy/MM/dd")
    private String startTime;         //活动开始时间
    @JsonFormat(pattern = "yyyy/MM/dd")
    private String endTime;           //活动结束时间
    private Date createTime;        //创建时间
    private Integer status;         //状态
    private String title_en;        //英文标题
    private String title_tai;       //泰语标题
    private String title_kh;        //柬埔寨标题
    private Integer xianshi;        //该活动是否显示在活动列表;1：显示，0：不显示
    private String url;             //活动连接
    private String detail_en;       //英文详情
    private String detail_tai;      //泰文详情
    private String detail_kh;       //柬埔寨详情

    @TableField(exist = false)
    private String date1;           //开始时间的字符串形式
    @TableField(exist = false)
    private String date2;           //结束时间的字符串形式
}
