package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ttw
 * @Date: 2021-10-10
 * @Description: 聊天详情
 */
@Data
@TableName(value = "chatdetail")
public class Chatdetail implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String chatId;      //chat表id
    private String kname;       //客服名
    private String uname;       //用户名
    private String kefumsg;     //客服发送的消息
    private String usermsg;     //用户发送的消息
    private Integer ptype;       //1是文字 2图片
    private String picture;     //图片
    private Integer mtype;      //消息类型（0：表示客服发给用户，1：表示用户发给客服，2：提示用户，我是客服？？？）
    private Date time;          //消息发送的时间
    private Integer status;         //未读消息（0：未读，1：已读)
    private Integer useStatus;      //用户是否已读（0：未读，1：已读)
}
