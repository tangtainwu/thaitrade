package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@TableName("lunbotu")
@Data
public class Lunbotu implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String title;           //标题
    private Integer type;           //0:内容 ,1:url链接
    private String message;         //描述
    private String picture;         //轮播图片路径
    private String url;             //url路径
    private Integer status;         //0禁用，1启用
    private Date createtime;        //创建时间
    private String title_en;        //英文标题
    private String title_tai;       //泰语标题
    private String title_kh;        //柬埔寨标题
    private String message_en;      //英文描述
    private String message_tai;     //泰语描述
    private String message_kh;      //柬埔寨描述
    private Integer sort;           //轮播图展示的顺序 （数字越小越靠前）
}
