
package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: Liao
 * @Date: 2021-10-19 10:57
 * @Description:
 */
@Data
@TableName("smsconfig")
public class SmsConfig implements Serializable {
    private String id;             //用户id
    private String appcode;           //用户名称
    private String host;           //用户手机号码，登录账号
    private String path;           //登录密码
    private String templetId;           //用户地址
}
