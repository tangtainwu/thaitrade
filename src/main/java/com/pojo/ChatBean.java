package com.pojo;

import lombok.Data;

/**
 * @Author:TTW
 * @Discription:webSoecket聊天发消息的格式
 */
@Data
public class ChatBean {
    private String name ;   //发送者名字
    private String kefu ;   //接受者名字
    private String msg ;//消息内容
    private Integer type ;   //消息类型 1：用户发给客服 2：客服发给用户
    private Integer ptype ; //1：文字 2：图片
}
