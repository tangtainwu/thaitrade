package com.pojo;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ttw
 * @Date: 2022-02-21
 * @Description: 弹窗广告
 */
@Data
@TableName(value = "advertise")
public class Advertise implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String picture;
    private String picture_en;
    private String picture_tai;
    private String picture_kh;
    private Integer status;
    private Date createTime;
}
