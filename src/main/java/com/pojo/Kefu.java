package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ttw
 * @Date: 2021-10-10
 * @Description:
 */
@Data
@TableName(value = "kefu")
public class Kefu implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String kefuName;        //客服名
    private String password;        //密码
    private Integer status;         //状态（0：禁用，1：启用)
    private String areaId;          //区域id

    @TableField(exist = false)
    private Area area;              //客服所属区域
}
