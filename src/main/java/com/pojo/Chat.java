package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: ttw
 * @Date: 2021-10-10
 * @Description: 客服和用户关联
 */
@Data
@TableName(value = "chat")
public class Chat implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String kefuName;    //客服名
    private String userName;    //用户名
    @TableField(exist = false)
    private int weidu;          //未读消息个数
    @TableField(exist = false)
    private Date latedate;      //最近聊天的时间
    @TableField(exist = false)
    private String uuname;      //用户名称
}
