
package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author: Liao
 * @Date: 2021-10-19 10:57
 * @Description:
 */
@Data
@TableName("users")
public class Users implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String uid;             //用户id
    private String uname;           //用户名称
    private String utel;           //用户手机号码，登录账号
    private String upsw;           //登录密码
    private String uaddress;           //用户地址
    private Integer ustatus;           //用户状态 1 使用  0 禁用
    private Integer type;           //用户类型：1为注册用户，2为游客用户
    private Date createTime;        //注册时间
}
