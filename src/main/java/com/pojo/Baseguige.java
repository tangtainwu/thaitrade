package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@Data
public class Baseguige implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private Integer id;
    private String name;
}