package com.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@Data
@TableName("goods")
public class Goods implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;             //商品id
    private String gName;           //商品名称
    @TableField(updateStrategy  = FieldStrategy.IGNORED)//修改时可以允许这个字段为空
    private BigDecimal grabPrice;   //抢购价人民币
    private String grabPriceDanwei;  //第一个抢购价的单位(中文）
    @TableField(updateStrategy  = FieldStrategy.IGNORED)//修改时可以允许这个字段为空
    private BigDecimal grabPriceSecond; //第二抢购价，人民币
    private String grabPriceSecondDanwei;   //第二抢购价单位
    private BigDecimal originalPrice;   //商品原价
    private BigDecimal gPrice;      //商品价格
    private String gTypeId;         //商品类别id
    private String gDetail;         //商品详情
    private String gDetail_en;      //英文商品详情
    private String gDetail_tai;     //泰语商品详情
    private String gDetail_kh;     //柬埔寨商品详情
    private Integer gStatus;        //上下架（0:下架，1：上架）
    private String gPicture;        //商品图片
    private String gDanwei;         //商品单位
    private Integer gSellnum;       //销售数量
    private Date createtime;        //商品新增时间
    private String gChandi;         //产地
    private String gJianjie;        //商品简介
    private String guige;           //规格
    private Integer gKucun;         //库存
    private String gPinpai;         //品牌
    private String gName_en;        //英文名字
    private String gName_tai;       //泰语名字
    private String gName_kh;       //柬埔寨名字
    private String guige_en;        //规格英文
    private String guige_tai;       //规格泰语
    private String guige_kh;       //规格柬埔寨
    private String gChandi_en;      //产地英文
    private String gChandi_tai;     //产地泰语
    private String gChandi_kh;     //产地柬埔寨
    private String gPinpai_en;      //品牌英文
    private String gPinpai_tai;     //品牌泰语
    private String gPinpai_kh;     //品牌柬埔寨
    private String gDanwei_en;      //单位英文
    private String gDanwei_tai;     //单位泰语
    private String gDanwei_kh;     //单位柬埔寨
    private String gJianjie_en;     //简介英文
    private String gJianjie_tai;    //简介泰语
    private String gJianjie_kh;    //简介柬埔寨
    private BigDecimal gPrice_en;   //美元价格
    private BigDecimal gPrice_tai;  //泰铢价格美元
    private BigDecimal gPrice_kh;  //柬埔寨价格美元
    @TableField(updateStrategy  = FieldStrategy.IGNORED)
    private BigDecimal grabPrice_en;            //抢购价美元
    private String grabPriceDanwei_en;          //英文第一抢购价单位
    @TableField(updateStrategy  = FieldStrategy.IGNORED)
    private BigDecimal grabPriceSecond_en;      //英文第二抢购价
    private String grabPriceSecondDanwei_en;    //英文第二抢购价单位
    @TableField(updateStrategy  = FieldStrategy.IGNORED)
    private BigDecimal grabPrice_tai;           //抢购价泰铢
    private String grabPriceDanwei_tai;         //泰铢第一抢购价单位
    @TableField(updateStrategy  = FieldStrategy.IGNORED)
    private BigDecimal grabPriceSecond_tai;     //泰铢第二抢购价
    private String grabPriceSecondDanwei_tai;   //泰铢第二抢购价单位
    @TableField(updateStrategy  = FieldStrategy.IGNORED)
    private BigDecimal grabPrice_kh;           //抢购价柬埔寨
    private String grabPriceDanwei_kh;         //柬埔寨第一抢购价单位
    @TableField(updateStrategy  = FieldStrategy.IGNORED)
    private BigDecimal grabPriceSecond_kh;     //柬埔寨第二抢购价
    private String grabPriceSecondDanwei_kh;   //柬埔寨第二抢购价单位
    private Integer weights;                    //商品权重
    private BigDecimal originalPrice_en;        //商品原价英文
    private BigDecimal originalPrice_tai;       //商品原价泰语（美元（
    private BigDecimal originalPrice_kh;       //商品原价柬埔寨 （美元（


    private Integer tuijian;       // 是否推荐

    @TableField(exist = false)
    private List goodsPictureList;    //商品图片集合
    @TableField(exist = false)
    private GoodsType goodsType;

}
