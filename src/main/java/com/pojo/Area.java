package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户客服区分的区域
 * @Author:ttw
 * @Date:2022-02-23
 */
@Data
@TableName(value = "area")
public class Area implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    private String areaName;    //区域名称
    private String areaDescribe;    //区域描述
    private Date createTime;    //创建时间
    private String areaName_en;
    private String areaName_tai;
    private String areaName_kh;
    @TableField(exist = false)
    private Integer numbers;    //客服数量
}
