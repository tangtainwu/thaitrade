package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("goods_picture")
public class GoodsPicture implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String gId;         //商品id
    private String gPicture;     //商品图片
    private Date createtime;    //创建时间
}
