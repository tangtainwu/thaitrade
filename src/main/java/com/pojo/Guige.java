package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@Data
@TableName("guige")
public class Guige implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;      //规格ID
    private String name;    //规格名称
    private String uid;
    private Integer gtype;       //类型（0:中文，1：英文，2：泰语）
}
