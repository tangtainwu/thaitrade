package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 英文商品轮播图
 * @Author:ttw
 * @Date:2022-01-05
 */
@Data
@TableName("goods_picture_en")
public class GoodsPictureEn implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String gId;         //商品id
    private String gPicture;     //商品图片
    private Date createtime;    //创建时间
}
