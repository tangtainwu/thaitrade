package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@Data
@TableName("goods_type")
public class GoodsType implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id ;
    /**
     * 名称
     */
    private String gname;
    /**
     * 上级id
     * null表示没有上级
     */
    private String superior_id;
    /**
     * 类别等级（1,2,3)
     */
    private Integer grade;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 英文名称
     */
    private String gname_en;
    /**
     * 泰语名字
     */
    private String gname_tai;
    /**
     * 柬埔寨名字
     */
    private String gname_kh;
    public GoodsType() { }
    /**
     * 父类别
     */
    @TableField(exist = false)
    private GoodsType parent ;
    private String icon ; // 类别图标
    /**
     * 所有的子类型
     */
    @TableField(exist = false)
    private List<GoodsType> children ;

    public GoodsType(String id, String gname, String superior_id, Date createTime, String icon,String gname_en,String gname_tai) {
        this.id = id;
        this.gname = gname;
        this.superior_id = superior_id;
        this.createTime = createTime;
        this.icon = icon;
        this.gname_en=gname_en;
        this.gname_tai = gname_tai;
    }
    /**
     * 该类型下所有的商品
     */
    @TableField(exist = false)
    private List<Goods> goods ;

    @Override
    public String toString() {
        return "GoodsType{" +
                "id=" + id +
                ", gname='" + gname + '\'' +
                ", superior_id=" + superior_id +
                ", createTime=" + createTime +
                ", icon=" + icon +
                ", grade=" + grade +
                ", gname_en='" + gname_en +'\''+
                ", gname_tai='" + gname_tai  +'\''+
                '}';
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getSuperior_id() {
        return superior_id;
    }

    public void setSuperior_id(String superior_id) {
        this.superior_id = superior_id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public GoodsType getParent() {
        return parent;
    }

    public void setParent(GoodsType parent) {
        this.parent = parent;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<GoodsType> getChildren() {
        return children;
    }

    public void setChildren(List<GoodsType> children) {
        this.children = children;
    }

    public List<Goods> getGoods() {
        return goods;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }
}