package com.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
/**
 * 数据库里不存在这张表
 *
 * @author lenovo
 */
@Data
public class TypeData implements Serializable {

    /**
     * id
     */
    private String id;

    /**
     * 名称
     */
    private String title;

    /**
     *
     */
    private String field;

    /**
     * 是否禁用
     */
    private boolean disabled;

    /**
     * 是否选中
     */
    private boolean checked;

    /**
     * 上级id
     */
    private String parent_ID;

    private boolean spread;

    private Integer grade;

    /**
     * 子节点
     */
    private List<TypeData> children = new ArrayList<>();

    public TypeData() {

    }

    public TypeData(String id, String title, String field, boolean disabled, boolean checked, boolean spread, List<TypeData> children, String parent_ID, Integer grade) {
        this.id = id;
        this.title = title;
        this.field = field;
        this.disabled = disabled;
        this.checked = checked;
        this.spread = spread;
        this.children = children;
        this.parent_ID = parent_ID;
        this.grade = grade;
    }

    /**
     * 将数据转换成树形结构的样式
     * @param parents 需要转换的数据
     * @param x x为2时：表示新增商品时查询所有类型，所有类型都是可点击的状态 x不为2时：表示三级类别不能点击
     * @return
     */
    public static List<TypeData> toTypeData(List parents, int x) {
        if(parents.size() == 0) {
            return new ArrayList<>() ;
        }
        List<TypeData> types = new ArrayList<>();
        if(parents.get(0) instanceof GoodsType) {
            for (Object obj : parents) {
                GoodsType g = (GoodsType) obj ;
                Boolean bool=false;
                if (x!=2){
                    bool= (g.getGrade()==3);
                }
                if (x==2){
                    bool= (g.getGrade()!=3);
                }
                TypeData t = new TypeData(g.getId(), g.getGname(), null, bool, false, false, new ArrayList<TypeData>(), g.getSuperior_id(),g.getGrade());
                types.add(t);
            }
        }
        return types;
    }

    /**
     * 改写的toString
     *
     * @return
     */
    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", field='" + field + '\'' +
                ", disabled=" + disabled +
                ", checked=" + checked +
                ", spread=" + spread +
                ", parent_ID=" + parent_ID +
                ", children=" + children +
                '}';
    }

    /**
     * 添加子节点
     *
     * @param z
     * @return
     */
    public TypeData addChildNode(TypeData z) {
        this.children.add(z);
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getParent_ID() {
        return parent_ID;
    }

    public void setParent_ID(String parent_ID) {
        this.parent_ID = parent_ID;
    }

    public void setGrade(Integer grade){
        this.grade = grade;
    }

    public Integer getGrade(){
        return grade;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    public List<TypeData> getChildren() {
        return children;
    }

    public void setChildren(List<TypeData> children) {
        this.children = children;
    }
}
