package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: ttw
 * @Date: 2021-10-6 10:57
 * @Description:
 */
@Data
@TableName("admin")
public class Admin implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;             //id
    private String nike;       //昵称
    private String username;   //用户名
    private String password;   //密码
}
