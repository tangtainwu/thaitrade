package com.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 主页关于的图片
 */
@Data
@TableName("guanyu")
public class Guanyu implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 图片路径
     */
    private String picture;

    /**
     * 1：品牌故事图片 2：联系我们图片
     */
    private Integer type;

    private String picture_en;

    private String picture_th;

    private String picture_kh;
}
