
var layer = layui.layer;
//得到工程路径
	function getRootPath() {
	//http://localhost:8083/uimcardprj/share/meun.jsp
	var curWwwPath = window.document.location.href;
	//uimcardprj/share/meun.jsp
	var pathName = window.document.location.pathname;
	var pos = curWwwPath.indexOf(pathName);
	//http://localhost:8083
	var localhostPaht = curWwwPath.substring(0, pos);
	// uimcardprj
	var projectName = pathName
			.substring(0, pathName.substr(1).indexOf('/') + 1);
	if (projectName == "/admin" || projectName=="/static" || projectName=="/weixin" || projectName=="/business")
		projectName = "";

	return (projectName);
}
var path = getRootPath();
var $ = layui.$;
$("#loginBtn").click(function () {
	var username = $("#LAY-user-login-username").val();
	var password = $("#LAY-user-login-password").val();
	var code2 = $("#loginCard").val();
	if(username==null||username==''){
		layer.msg("用户名不能为空");
		return;
	}
	if(password==null||password==''){
		layer.msg("密码不能为空");
		return;
	}
	$.ajax({
		url: '/admin/login',
		type: 'post',
		data: {
			username:username,
			password:password,
		},
		success:function (data) {
			if(data.code==200){
			    localStorage.setItem("admin_id",data.data.id);
				layer.msg("登录成功",{
					time:2000,
					end:function () {

						 	window.location.href="/admin/main.html";

					}
					}
				);

			}else{
					layer.msg(data.msg,{time:2000});
			}
			}
		,
		erorr:function(){
			layer.msg("网络异常");
		}
	});
});

$(window).keypress(function (event) {
	if(event.which === 13){
		$('#loginBtn').click();
	}
})

