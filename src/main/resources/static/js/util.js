var host = "";

host= getRootPath();
function getRootPath() {
	//http://localhost:8083/uimcardprj/share/meun.jsp
	var curWwwPath = window.document.location.href;
	//uimcardprj/share/meun.jsp
	var pathName = window.document.location.pathname;
	var pos = 0;
	if (pathName=="/"){
		pos = (curWwwPath.toString().length)-1;
	}else{
		pos = curWwwPath.indexOf(pathName);
	}
	//http://localhost:8083
	var localhostPaht = curWwwPath.substring(0, pos);
	// uimcardprj
	var projectName = pathName
			.substring(0, pathName.substr(1).indexOf('/') + 1);
	if (projectName == "/pc" || projectName == "/admin" || projectName == "/weixin"  || projectName == "/wap")
		projectName = "";

	return (localhostPaht);
}
function getQuery(variable)
{
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){return pair[1];}
	}
	return(false);
}
var language_pack = {
		    now_lang : 0, // 0:ch,1:en,2:tai
		    loadProperties : function(new_lang){
		    	console.log(new_lang);
		        var self = this;
		        var tmp_lang = '';
		        if(new_lang == 0){
		            tmp_lang = 'zh';
		            $('body').removeClass().addClass('zh');
		        }else if(new_lang== 1){
		            tmp_lang = 'en';
		            $('body').removeClass().addClass('en');
		        }else if(new_lang==2){
					tmp_lang = 'th';
					$('body').removeClass().addClass('th');
				}else if (new_lang==3){
					tmp_lang = 'kh';
					$('body').removeClass().addClass('kh');
				}
		        jQuery.i18n.properties({//加载资浏览器语言对应的资源文件
		            name: 'string', //资源文件名称
		            path:'language/', //资源文件路径
		            language: tmp_lang,
		            cache: false,
		            mode:'map', //用Map的方式使用资源文件中的值
		            callback: function() {//加载成功后设置显示内容
		                for(let i in $.i18n.map){
		                    $('[data-lang="'+i+'"]').text($.i18n.map[i]);
		                    $('[data-holder="'+i+'"]').attr("placeholder",$.i18n.map[i]);
		                }
						// for(let i in $.i18n.map){
						// 	let t =$('[placeholder="'+i+']').attr('placeholder');
						// 	$('[placeholder="'+i+']').attr('placeholder',t);
						// }
		                // document.title = $.i18n.map['string_title'];
		            }
		        });
		        self.now_lang = new_lang;
		    }
		}
// 登录页面切换
$(function () {
	$("ul.tabBoxSwitchUl").on('click', 'li', function () {
		let i = $(this).index();
		$(this).addClass("tab-active").siblings('li').removeClass("tab-active");
		$("div.tabcont").eq(i).addClass("active").siblings().removeClass("active");
	});

})
// 获取国际区号
var data  = {
"CountryNum":[
	[
		{
			"countryName": " 老挝",
			"number": " +856"
		},
		{
			"countryName": " 中国",
			"number": " +86"
		},
		{
			"countryName": " 美国",
			"number": " +1"
		},
		{
			"countryName": " 泰国",
			"number": " +66"
		},
		{
			"countryName": " 日本",
			"number": " +81"
		},
		{
			"countryName": " 中国香港",
			"number": " +852"
		},
		{
			"countryName": " 中国澳门",
			"number": " +853"
		},
		{
			"countryName": " 中国台湾",
			"number": " +886"
		},
		{
			"countryName": " 马来西亚",
			"number": " +60"
		},
		{
			"countryName": " 印度尼西亚",
			"number": " +62"
		},
		{
			"countryName": " 菲律宾",
			"number": " +63"
		},
		{
			"countryName": " 柬埔寨",
			"number": " +855"
		},
		{
			"countryName": " 韩国",
			"number": " +82"
		},
		{
			"countryName": " 越南",
			"number": " +84"
		},
		{
			"countryName": " 缅甸",
			"number": " +95"
		},
	],
	[
		{
			"countryName": "Laos",
			"number": " +856"
		},
		{
			"countryName": "China",
			"number": " +86"
		},
		{
			"countryName": "USA",
			"number": " +1"
		},
		{
			"countryName": "Thai",
			"number": " +66"
		},
		{
			"countryName": "Japan",
			"number": " +81"
		},
		{
			"countryName": "Hong Kong",
			"number": " +852"
		},
		{
			"countryName": "Macau",
			"number": " +853"
		},
		{
			"countryName": "Taiwan",
			"number": " +886"
		},
		{
			"countryName": "Malaysia",
			"number": " +60"
		},
		{
			"countryName": "Indonesia",
			"number": " +62"
		},
		{
			"countryName": "Philippine",
			"number": " +63"
		},
		{
			"countryName": "Cambodia",
			"number": " +855"
		},
		{
			"countryName": "South Korea",
			"number": " +82"
		},
		{
			"countryName": "Vietnam",
			"number": " +84"
		},
		{
			"countryName": "Myanmar",
			"number": " +95"
		},
	],
	[
		{
			"countryName": "ลาว",
			"number": " +856"
		},
		{
			"countryName": "จีน",
			"number": " +86"
		},
		{
			"countryName": "อเมริกา",
			"number": " +1"
		},
		{
			"countryName": "ประเทศไทย",
			"number": " +66"
		},
		{
			"countryName": " ี่ปุ่น",
			"number": " +81"
		},
		{
			"countryName": "ฮ่องกง",
			"number": " +852"
		},
		{
			"countryName": "มาเก๊า",
			"number": " +853"
		},
		{
			"countryName": "ไต้หวัน",
			"number": " +886"
		},
		{
			"countryName": "มาเลเซีย",
			"number": " +60"
		},
		{
			"countryName": "อินโดนีเซีย",
			"number": " +62"
		},
		{
			"countryName": "ฟิลิปปินส์",
			"number": " +63"
		},
		{
			"countryName": "กัมพูชา",
			"number": " +855"
		},
		{
			"countryName": "เกาหลีใต้",
			"number": " +82"
		},
		{
			"countryName": "เวียดนาม",
			"number": " +84"
		},
		{
			"countryName": "พม่า",
			"number": " +95"
		},
	],
]

}
$(function () {

	$("button.selectBtn").click(function (e) {
	    if ($(".selectConentent").is(':hidden')) {
	        $(".selectConentent").show();

	    } else {
	        $(".selectConentent").hide();
	    }
	    $(document).one('click', function () {
	        $(".selectConentent").hide();
	    });
	    e.stopPropagation();
	});
	$(".selectConentent").on('click', function (e) {
	    e.stopPropagation();
	})
	var n =0;
	if(localStorage.getItem("yuyan")=='en')
		n=1;
	else if(localStorage.getItem("yuyan")=='th')
		n=2;
	$.each(data.CountryNum[n], function (i, item) {
		// console.log(item.countryName, item.number);
		let btns = " <button class='phone-btn selectBtn select-option' type='button' data-type='option'" + "data-id=" + i + ">" + item.countryName + "   " + item.number + "</button>";
		$(".selectOptions").append(btns);
		chooseBtn();
		// console.log(btns);
	});

	// 国际区号点击事件
	function chooseBtn() {
		$("button[data-type='option']").each(function () {
			$(this).click(function () {
				let txt = $(this).text();
				$("button[data-type='selected']").attr("data-fid", $(this).index());
				$("button[data-type='selected'] span").text(txt);
				$(".selectConentent").hide();
				$(".selectOptions").scrollTop($(this).index() * 40);
			});
			$(this).hover(function () {
				$(this).css("background-color", "#f6f6f6");
			}, function () {
				$(this).css("background-color", "#ffffff");
			});
		});
	};
	// 如果已登录，显示用户名
	if(localStorage.getItem("userid")){
		if(localStorage.getItem("userid").startsWith("+")){
			$(".topbg .loginBtn").hide();
			$(".topbg .loginBtn").parent().append('<span style="color: white">'+localStorage.getItem("userid")+'</span>');
			$(".topbg .loginBtn").parent().append('<span style="color: white;margin-left: 10px;cursor: pointer;" onclick="loginout()" data-lang="login_out" >'+'注销'+'</span>');
		}
	}else{
		$(".topbg .loginBtn").show();
	}
})
function loginout() {
	$(".topbg .loginBtn").show();
	$(".topbg .loginBtn").parent().find('span').remove();
	localStorage.removeItem("userid");
	localStorage.removeItem("userName");
	location.href = "/index.html";
}


