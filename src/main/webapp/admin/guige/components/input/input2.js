Vue.component("my-input" , {
    template:'<input @blur="change" class="my-input" v-model="value" :placeholder="place"/>' ,
    name:"my-input" ,
    data(){
        return {

        }
    } ,
    props:{
        value:{
            type:String
        } ,
        type:{
            type:String ,
            default(){
                return "text"
            }
        } ,
        place:{
            type:String ,
            default(){
                return "อินพุตข้อมูลจำเพาะ"
            }
        }
    },
    methods:{
        change(){
            this.$emit("change", {
                value:this.value
            })
        }
    }
})