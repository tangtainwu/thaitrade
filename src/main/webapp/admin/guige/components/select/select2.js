Vue.component("my-select" , {
    template:'<div class="zq-drop-list" @mouseover="onDplOver($event)" @mouseout="onDplOut($event)">'+
        '<span>{{dplLable}}<i></i></span>'+
        '<ul v-dpl>'+
        '<li v-for="(item, index) in dataList" :key="index" @click="onLiClick(index, $event)">{{item[labelProperty]}}</li>'+
        '</ul>'+
        '</div>' ,
    name:"my-select" ,
    data(){
        return {

        }
    } ,
    props:{
        dataList:{
            type:Array,
            default(){
                return [
                    {name: "ตัวเลือกที่ 1"},
                    {name: "ตัวเลือกที่ 2"}
                ]
            }
        },
        labelProperty:{
            type:String,
            default(){ return "name" }
        } ,
        activeIndex:{
            type:Number ,
            default(){
                return 0
            }
        } ,
        name:{
            type:String
        }
    },
    directives:{
        dpl:{
            bind(el){
                el.style.display = "none";
            }
        }
    },
    methods:{
        onDplOver(event){
            let ul = event.currentTarget.childNodes[1];
            ul.style.display = "block";
        },
        onDplOut(event){
            let ul = event.currentTarget.childNodes[1];
            ul.style.display = "none";
        },
        onLiClick(index){
            let path = event.path || (event.composedPath && event.composedPath()) //兼容火狐和safari
            path[1].style.display = "none";
            this.activeIndex = index;
            this.name = this.dataList[index][this.labelProperty]
            this.$emit("change", {
                index:index,
                value:this.dataList[index]
            })
        }
    } ,
    computed:{
        dplLable(){
            //return this.dataList[this.activeIndex][this.labelProperty]
            return this.name
        }
    }
})