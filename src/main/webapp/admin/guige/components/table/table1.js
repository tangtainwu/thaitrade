Vue.component("my-table" , {
    template:'<div>'+
        '<table border="1" cellpadding="0" cellspacing="0" class="my-table">'+
        '<tr style="height:30px;"><td style="width:90px;">Specification picture</td><td v-for="item in names">{{item}}</td><td style="width:140px;">price</td></tr>'+
        '<tr v-for="(row,index) in rows*msg[0]"><td v-if="Math.abs(index - msg[0]) % msg[0] == 0" class="table-img" style="width:90px;" :rowspan="msg[0]"><div @click="upload(index)">' +
        '<span v-if="values[parseInt(index / msg[0])].img == \'\'">+</span><img v-if="values[parseInt(index / msg[0])].img != \'\'" :src="values[parseInt(index / msg[0])].img.startsWith(\'images\') ? \'/\'+values[parseInt(index / msg[0])].img : \'/tmp/\'+values[parseInt(index / msg[0])].img" /></div></td>' +
        '<td v-if="Math.abs(index - msg[idx]) % msg[idx] == 0" v-for="(col,idx) in colms" :rowspan="msg[idx]">' +
        '{{current(index , idx)}}' +
        '</td><td rowspan="1"><input :value="jiage1(index)" @change="jiage($event , index)"></td></tr>'+
        '</table><input @change="file_select" type="file" style="display:none;" name="photo" id="photo" value="" placeholder="">'
        +'</div>' ,
    name:"my-table" ,
    data(){
        return {
            index:0
        }
    } ,
    props:{
        names:{
            type:Array
        } ,
        values:{
            type:Array
        }
    },
    created(){
        // console.log(this.values)
    } ,
    methods:{
        upload:function(index) {
            this.index = index
            $("#photo").click()
        } ,
        jiage1:function(index) {
            switch (this.colms) {
                case 1:
                    return this.values[parseInt(index / this.msg[0])].jiage
                    break
                case 2:
                    return this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]].jiage
                    break
                case 3:
                    return this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]]["children"][index % this.msg1[2]].jiage
                    break
            }
        } ,
        jiage:function(e , index){
            var val = e.srcElement.value
            switch (this.colms) {
                case 1:
                    this.values[parseInt(index / this.msg[0])].jiage = val
                    break
                case 2:
                    this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]].jiage = val
                    break
                case 3:
                    this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]]["children"][index % this.msg1[2]].jiage = val
                    break
            }
        } ,
        kucun1:function(index){
            switch (this.colms) {
                case 1:
                    return this.values[parseInt(index / this.msg[0])].kucun
                    break
                case 2:
                    return this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]].kucun
                    break
                case 3:
                    return this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]]["children"][index % this.msg1[2]].kucun
                    break
            }
        } ,
        kucun:function(e , index){
            var val = e.srcElement.value
            switch (this.colms) {
                case 1:
                    this.values[parseInt(index / this.msg[0])].kucun = val
                    break
                case 2:
                    this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]].kucun = val
                    break
                case 3:
                    this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]]["children"][index % this.msg1[2]].kucun = val
                    break
            }
        } ,
        file_select:function(){
            layer.load()
            this.parseData()
        } ,
        parseData:function() {
            var self = this
            var formData = new FormData();
            console.log()
            if (!$("#photo")[0].files[0].type.startsWith("image")) {
                layer.closeAll()
                layer.msg("Only upload pictures")
                return
            }
            formData.append("file", $("#photo")[0].files[0]);
            $.ajax({
                url: '/upload.do/upload', /*接口域名地址*/
                type: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function (res) {
                    self.values[parseInt(self.index / self.msg[0])].img = res
                    // if(res.data["code"]=="succ"){
                    //     alert('成功');
                    // }else if(res.data["code"]=="err"){
                    //     alert('失败');
                    // }else{
                    //     console.log(res);
                    // }
                    layer.closeAll()
                    $("#photo").val("")
                },
                fail: function (err) {
                    layer.closeAll()
                }
            })
        } ,
        walkinto:function(data , idx , col , index){
            if(col == 0) {
                return data.name ;
            }
            if(data["children"].length == 0) {
                return
            }
            if(idx + 1 == col) {
                // console.log(data["children"][index].name)
                return data["children"][index].name
            }else{
                for(var i in data["children"]) {
                    return this.walkinto(data["children"][i] , idx+1 , col , index)
                }
            }
        } ,
        current:function(index , idx){
            switch (idx) {
                case 0:
                    return this.values[parseInt(index / this.msg[0])].name
                    break
                case 1:
                    return this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]].name
                    break
                case 2:
                    return this.values[parseInt(index / this.msg[0])]["children"][parseInt(index / this.msg[1]) % this.msg1[1]]["children"][index % this.msg1[2]].name
                    break
            }
            // return this.walkinto(line , 0 , col , index)
        }
    } ,
    computed:{
        rows:function(){
            return this.values.length
        } ,
        colms:function(){
            if(this.values.length == 0) {
                return 0
            }else{
                if(this.values[0]["children"].length == 0) {
                    return 1
                }else{
                    if(this.values[0]["children"][0]["children"].length == 0) {
                        return 2
                    }else{
                        return 3
                    }
                }
            }
        } ,
        msg:function(){
            var res = []
            if(this.values.length == 0) {
                res.push(1)
            }else{
                if(this.values[0]["children"].length == 0) {
                    res.push(1)
                    res.push(1)
                }else{
                    if(this.values[0]["children"][0]["children"].length == 0) {
                        res.push(this.values[0]["children"].length)
                        res.push(1)
                    }else{
                        res.push(this.values[0]["children"][0]["children"].length * this.values[0]["children"].length)
                        res.push(this.values[0]["children"][0]["children"].length)
                        res.push(1)
                        console.log("msg" , res)
                    }
                }
            }
            return res
        } ,
        msg1:function(){
            var res = []
            if(this.values.length == 0) {
                res.push(this.values.length)
            }else{
                if(this.values[0]["children"].length == 0) {
                    res.push(this.values.length)
                    res.push(1)
                }else{
                    if(this.values[0]["children"][0]["children"].length == 0) {
                        res.push(this.values.length)
                        res.push(this.values[0]["children"].length)
                    }else{
                        res.push(this.values.length)
                        res.push(this.values[0]["children"].length)
                        res.push(this.values[0]["children"][0]["children"].length)
                    }
                }
            }
            return res
        }
    }
})